import { getEventQuotes } from './eventDates';

const cachedVersion = '3';

export const loadQuotesFromStorage = (flavor) => {
  const eventQuotes = getEventQuotes(flavor);

  if (eventQuotes) {
    return eventQuotes;
  }

  const quotes = JSON.parse(localStorage.getItem(`quotes${cachedVersion}`));

  if (quotes) {
    return quotes;
  }

  return null;
};

export const saveQuotesToStorage = (data) => {
  const quotes = JSON.stringify(data);

  localStorage.setItem(`quotes${cachedVersion}`, quotes);
};

export const clearQuotesFromStorage = () => {
  localStorage.removeItem(`quotes${cachedVersion}`);
};
