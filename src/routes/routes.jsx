import React from 'react';

import loadable from '@loadable/component';
import Loading from '../components/Loading';

const LoadingComponent = () => <Loading />;

export const HomePage = loadable(() => import('../views/HomePage'), { LoadingComponent });
export const SubforumPage = loadable(() => import('../views/SubforumPage'), {
  LoadingComponent,
});
export const SubforumRulesPage = loadable(() => import('../views/SubforumRulesPage'), {
  LoadingComponent,
});
export const ThreadPage = loadable(() => import('../views/ThreadPage'), { LoadingComponent });
export const ThreadCreationPage = loadable(() => import('../views/ThreadCreationPage'), {
  LoadingComponent,
});
export const ThreadRulesPage = loadable(() => import('../views/ThreadRulesPage'), {
  LoadingComponent,
});
export const EventsPage = loadable(() => import('../views/EventsPage'), { LoadingComponent });
export const UserSettingsPage = loadable(() => import('../views/UserSettingsPage'), {
  LoadingComponent,
});
export const Subscriptions = loadable(() => import('../views/AlertsList'), { LoadingComponent });
export const Rules = loadable(() => import('../views/RulesPage'), { LoadingComponent });
export const UserSetup = loadable(() => import('../views/UserSetupPage'), { LoadingComponent });
export const UserProfile = loadable(() => import('../views/UserProfile/index'), {
  LoadingComponent,
});
export const LoginPage = loadable(() => import('../views/LoginPage'), { LoadingComponent });
export const KnockoutBBSyntaxPage = loadable(() => import('../views/KnockoutBBSyntaxPage'), {
  LoadingComponent,
});
export const Moderate = loadable(() => import('../views/Moderation'), { LoadingComponent });

// messages
export const MessagesPage = loadable(() => import('../views/MessagesPage'), {
  LoadingComponent,
});

export const DocsPage = loadable(() => import('../views/DocsPage'), {
  LoadingComponent,
});

export const Logout = loadable(() => import('../views/Logout'), {
  LoadingComponent,
});

// thread search
export const ThreadSearchPage = loadable(() => import('../views/ThreadSearchPage'), {
  LoadingComponent,
});

// changelog
export const ChangelogPage = loadable(() => import('../views/ChangelogPage'), {
  LoadingComponent,
});

// calendar events
export const CalendarPage = loadable(() => import('../views/CalendarPage'), {
  LoadingComponent,
});

// community team
export const CommunityTeamPage = loadable(() => import('../views/CommunityTeamPage'), {
  LoadingComponent,
});
