import config from '../../config';
import { getEventHeaderLogo } from '../utils/eventDates';

// ### Filtered subforums for popular/latest ###
/**
 * @returns {number[]}
 */
export const loadFilteredSubforumsFromStorage = () => {
  const jsonArray = localStorage.getItem('filtered-subforums');

  if (!jsonArray) {
    return [];
  }

  try {
    return JSON.parse(jsonArray);
  } catch {
    return [];
  }
};

/**
 * @param {number[]} filteredSubforums
 */
export const setFilteredSubforumsToStorage = (filteredSubforums) => {
  localStorage.setItem('filtered-subforums', JSON.stringify(filteredSubforums));
};

// ### Theme ###
export const loadThemeFromStorage = () => {
  const theme = localStorage.getItem('theme');
  if (localStorage.getItem('customColors')) {
    return 'custom';
  }
  if (theme === 'device') {
    return window.matchMedia('(prefers-color-scheme: dark)').matches ? 'dark' : 'light';
  }
  return theme || 'dark';
};

export const setThemeToStorage = (theme) => {
  localStorage.setItem('theme', theme);
};

// ### Custom Theme ###
export const loadCustomThemeFromStorage = () => {
  const customTheme = localStorage.getItem('customTheme');

  return customTheme;
};
export const setCustomThemeToStorage = (customTheme) => {
  localStorage.setItem('customTheme', customTheme);
};
export const deleteCustomThemeFromStorage = () => {
  localStorage.removeItem('customTheme');
};

// ### Scale ###
export const loadScaleFromStorage = () => {
  const scale = localStorage.getItem('scale');
  return scale || 'medium';
};
export const setScaleToStorage = (scale) => {
  localStorage.setItem('scale', scale);
};

// ### Width ###
export const loadWidthFromStorage = () => {
  const width = localStorage.getItem('width');
  return width || 'wide';
};
export const setWidthToStorage = (width) => {
  localStorage.setItem('width', width);
};

// ### AutoSubscribe ###
export const loadAutoSubscribeFromStorage = () => {
  const autoSubscribe = localStorage.getItem('autoSubscribe');
  return autoSubscribe || 'true';
};
export const setAutoSubscribeToStorage = (autoSubscribe) => {
  localStorage.setItem('autoSubscribe', autoSubscribe);
};
export const loadAutoSubscribeFromStorageBoolean = () => loadAutoSubscribeFromStorage() === 'true';

// Remember limited users who the client has allowed media display for. This means that posts
// from limited users will not have media automatically spoilered.
const approvedUserIdsStorageKey = 'approved-users';

/**
 * @returns {number[]}
 */
export const loadApprovedUserIdsFromStorage = () => {
  const jsonArray = localStorage.getItem(approvedUserIdsStorageKey);

  if (!jsonArray) {
    return [];
  }

  try {
    return JSON.parse(jsonArray);
  } catch {
    return [];
  }
};

/**
 * @param {number[]} cleanUserIds
 */
export const setApprovedUserIdsToStorage = (cleanUserIds) => {
  localStorage.setItem(approvedUserIdsStorageKey, JSON.stringify(cleanUserIds));
  window.dispatchEvent(new Event('approvedUserIds'));
};

// ### Misc
export const getLogoPath = (flavor) => {
  const theme = loadThemeFromStorage();

  const eventLogo = getEventHeaderLogo(flavor);

  if (eventLogo) {
    if (theme in eventLogo) {
      return eventLogo[theme];
    }
    return eventLogo.default;
  }

  if (config.qa) {
    return '/static/logo_qa.png';
  }

  return null; // Fall back to default SVG that will be recoloured accordingly (StyleableLogo)
};
