import { BatchDeleteRequest } from 'knockout-schema';
import { authPost, authDelete, authGet } from './common';

export const createAlertRequest = async (threadId: number, lastPostNumber: number) => {
  const requestBody = {
    lastPostNumber,
    threadId,
  };

  const response = await authPost({ url: '/v2/alerts', data: requestBody });

  return response;
};

export const deleteAlertRequest = async ({ threadId }) => {
  if (!threadId) return { error: 'Invalid thread id' };

  const requestBody = {
    threadId,
  };

  const response = await authDelete({ url: `/v2/alerts/${threadId}`, data: requestBody });

  return response;
};

export const batchDeleteAlertsRequest = async ({ threadIds }: BatchDeleteRequest) => {
  const requestBody = { threadIds };

  const response = await authPost({ url: '/v2/alerts/batchDelete', data: requestBody });

  return response;
};

export const getAlerts = async (hideNsfw, page = 1) => {
  const user = localStorage.getItem('currentUser');

  if (!user) {
    return { alerts: [] };
  }
  try {
    const results = await authGet({ url: `/v2/alerts/${page}${hideNsfw ? '?hideNsfw=1' : ''}` });
    return results.data;
  } catch (err) {
    return [];
  }
};
