/* eslint-disable import/prefer-default-export */
import axios from 'axios';
import { User } from 'knockout-schema';
import config from '../../config';

export const getCommunityTeam = async (): Promise<Array<User>> => {
  try {
    const results = await axios.get(`${config.apiHost}/v2/community-team`);
    return results.data;
  } catch (err) {
    return [];
  }
};
