import axios from 'axios';
import config from '../../config';
import { authPost } from './common';

export const getLatestMotd = async () => {
  const results = await axios.get(`${config.apiHost}/motd`);

  let motd = results.data;
  if (motd === undefined) {
    motd = {
      message: '',
      id: 0,
    };
  }

  return motd;
};

export const createMotd = async (data) => {
  try {
    const results = await authPost({ url: '/motd', data });

    return results.data;
  } catch (err) {
    console.error(err);
    throw err;
  }
};
