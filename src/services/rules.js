import axios from 'axios';

import config from '../../config';
import { authDelete, authPut } from './common';

export const getSiteRules = async () => {
  const res = await axios.get(`${config.apiHost}/v2/rules`);
  const { data } = res;

  return data;
};

export const getThreadRules = async (threadId) => {
  const res = await axios.get(`${config.apiHost}/v2/rules`, {
    params: { rulableType: 'Thread', rulableId: threadId },
  });
  const { data } = res;

  return data;
};

export const getSubforumRules = async (subforumId) => {
  const res = await axios.get(`${config.apiHost}/v2/rules`, {
    params: { rulableType: 'Subforum', rulableId: subforumId },
  });
  const { data } = res;

  return data;
};

export const updateRule = async (ruleId, category, title, description) => {
  const res = await authPut({
    url: `/v2/rules/${ruleId}`,
    data: { category, title, description },
  });
  return res.data;
};

export const deleteRule = async (ruleId) => {
  const res = await authDelete({ url: `/v2/rules/${ruleId}` });
  return res.data;
};
