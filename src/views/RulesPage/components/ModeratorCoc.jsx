import React from 'react';
import { Helmet } from 'react-helmet';
import styled from 'styled-components';

const ModeratorCoc = () => {
  return (
    <StyledModCoc>
      <Helmet>
        <title>Moderator Code of Conduct - Knockout!</title>
      </Helmet>
      <h1 className="underline">Moderator Code of Conduct</h1>
      <p>
        The purpose of this page is to serve as a general guide for all new/existing moderators, and
        is publicly shared to help people understand what rules a moderator operates under.
      </p>
      <p>
        Before we begin, congratulations on your appointment as a Moderator! Thank you for
        dedicating your free time to make the forums better.
      </p>
      <section>
        <h2 className="underline-small">Our Objective</h2>

        <p>
          It&apos;s important to say that our mission for Knockout does not include moderators
          acting as thought-police. As long as members are discussing things on-topic, in a
          respectful manner and without breaking any other rules, they should be free to share their
          own opinions. Members will naturally rate them dumb, reply to them with their
          disagreements and so on as part of the discussion, not everyone agrees with anyone. If all
          mods were to ban people because their opinions are not in line with their own, we&apos;ll
          soon have to ban everyone, since we all have radically different views.
        </p>
        <p>
          However, there is a limit. If people are sharing harmful rhetoric that targets any group
          of members/demographics, a moderator will be required to step in and assess the situation.
          The moderator will also be required to discuss any action with the rest of the moderation
          team.
        </p>
        <p>
          Members should always be respectful, never attack other members, not incite violence, but
          should attempt to win an argument through rational and objective arguments. If you want to
          say gamerwords there are other places, and we simply won&apos;t accept any of that here.
        </p>
      </section>
      <section>
        <h2 className="underline-small">Duties and Responsibilities</h2>
        <p>
          All Moderators are expected to join the Official Knockout Discord server. This is a vital
          part of being able to discuss concerns or issues with the Moderation Team.
        </p>
        <p>
          A moderator&apos;s first and foremost duty is to ensure unity and togetherness both in
          discussions and between members, with the well being of members as the main consideration
          when moderating.
        </p>
        <p>
          The moderation team exists in a fully flat structure. That is, any single moderator has
          equal power and decision making capability as any other. Policy changes proposed by the
          moderation team must have a majority vote within the team, and any larger changes must
          have a super-majority vote.
        </p>
        <p>
          Moderators in their probationary period will be assigned the role of
          &quot;Moderator-in-Training&quot; where they are effectively a full time moderator with
          the caveat that they are expected to discuss what they believe is the correct action to
          take in response to reports or posts to the rest of the moderator team, and afterward take
          action based on that discussion. If no other moderators are available and the matter is
          time-sensitive, it is acceptable for the moderator-in-training to act on their own and
          discuss with the team after.
        </p>
        <p>
          Moderators are expected to check reports on a regular basis, ensuring that they do not
          stack and that they are dealt with on a fair and consistent basis.
        </p>
        <p>
          Moderators are expected to communicate with other moderators in the community team channel
          on Discord frequently and discuss possible bans and other moderator actions with them.
          Moderators are required to notify other fellow moderators if they are due to go on an
          extended leave of absence.
        </p>
        <p>
          Moderators are expected to know and understand all of the rules, be aware of any changes
          made to rules when they are made, and only ban in line with the rules.
        </p>
        <p>
          Moderators should be ready to seek and recognize the advice of experts within the
          moderation team on a particular matter before taking action if it is relevant to them, and
          if there are none available, discuss and research amongst the team to come up with a FAIR
          and INFORMED course of action.
        </p>
        <p>
          No moderator shall unilaterally overrule the action taken by another moderator without
          consensus discussed with at least two other moderators. If the issuing moderator agrees
          with the change, the action may be overruled without this consensus. Additionally, if the
          overruling is under a time-sensitive scope, only a single other moderator is needed for
          overruling, but a continued discussion is still required afterwards.
        </p>
      </section>
      <section>
        <h2 className="underline-small">Honesty and Integrity</h2>
        <p>
          Moderators must act with honesty and integrity at all times. They should be sincere and
          truthful, showing confidence in doing what they believe to be right.
        </p>
        <p>
          Moderators should not solicit or readily accept the offer of any gift or gratuity that
          could compromise their impartiality. In the interests of transparency, any gifts given
          must be declared to the moderator team.
        </p>
        <p>
          Moderators should not have any undisclosed alternate accounts on the forums, no matter the
          purpose of said accounts.
        </p>
        <p>
          Moderators must disclose when they may be compromised, conflicted, or emotionally charged
          in their decision making, and must yield to another moderator on the team if this is the
          case.
        </p>
        <p>
          Moderators are accountable to the community. All actions undertaken by Moderators are
          recorded in the Event Log. Moderators should not shy away from criticism, but rather
          explain their rationale when challenged.
        </p>
      </section>
      <section>
        <h2 className="underline-small">Standards of Behaviour</h2>
        <p>
          Moderators must lead by example in their posts and threads. Members take their cues from
          how the Moderators post.
        </p>
        <p>
          The tone of Moderators’ posts must be suitable for the given situation. Good humour is
          expected, but must also be used appropriately. Remember that whatever post you makes is
          representative of all the moderator team, and by extension Knockout.
        </p>
        <p>
          Bans must always be justifiable and proportionate. Moderators must take into account both
          the member (time on the forum, number of bans, overall quality of posts) and the content
          of the bannable material (are they flaming with 1 vulgar word or are they flaming with
          multiple vulgarities and deragitories?)
        </p>
        <p>
          If you take an issue with a Moderator’s behaviour, please discuss it with them in a
          constructive manner. If you feel the behaviour is beyond this, please notify multiple
          members of the moderation team so it can be discussed internally and a course of action
          can be taken.
        </p>
        <p>
          In regard to situations that expand beyond the confines of the forum, moderators should
          stop engaging within the situation and discuss how to handle it best amongst the other
          moderators. If a definitive answer can not be drawn from this conversation then a majority
          vote between the moderation team will determine the action. This includes dm’s on
          discord/steam/twitter/other social media.
        </p>
        <p>
          While we cannot control what members do outside of the forum we can control what we do.
          Remember that even outside of the forum, while running your forum username, you are acting
          as a representative of the Knockout community. While operating under your forum username
          it is expected, though not required, for you to maintain similar conduct as you would on
          the forums. If actions are taken that may damage the reputation of both the moderator team
          and/or Knockout a discussion will be had in the moderator chat about the best course of
          action in handling such an event.
        </p>
        <p>
          Moderators are expected to treat members with respect, both in public and private
          conversation. Moderators will not talk about other members in any derogatory fashion.
        </p>
        <p>
          Remember, we are all a team. We must all work together and be on the same page as much as
          possible. When in doubt, ask questions!
        </p>
      </section>
    </StyledModCoc>
  );
};

const StyledModCoc = styled.div`
  p {
    max-width: 1000px;
  }
`;
export default ModeratorCoc;
