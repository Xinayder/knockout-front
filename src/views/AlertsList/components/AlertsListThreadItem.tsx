import React from 'react';
import { ThreadWithLastPost } from 'knockout-schema';
import ThreadItem from '../../../components/ThreadItem';

interface AlertsListThreadItemProps {
  thread: ThreadWithLastPost;
  markUnreadAction: () => void;
  checkboxAction: (checked: boolean) => void;
  checked: boolean;
}

const AlertsListThreadItem = ({
  thread,
  markUnreadAction,
  checkboxAction,
  checked,
}: AlertsListThreadItemProps) => {
  return (
    <ThreadItem
      thread={thread}
      markUnreadAction={markUnreadAction}
      markUnreadText="Unsubscribe"
      showTopRating={false}
      includeCheckbox
      checkboxAction={checkboxAction}
      checked={checked}
    />
  );
};

export default AlertsListThreadItem;
