import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import styled from 'styled-components';
import dayjs from 'dayjs';
import relativeTime from 'dayjs/plugin/relativeTime';
import { useDispatch, useSelector } from 'react-redux';
import Post from '../../../components/Post';
import {
  createUserProfileComment,
  deleteUserProfileComment,
  getUserPosts,
  getUserProfileComments,
  getUserThreads,
} from '../../../services/user';
import ThreadItem from '../../../components/ThreadItem';
import { ThemeFontSizeLarge, ThemeFontSizeMedium } from '../../../utils/ThemeNew';
import UserAvatar from '../../../components/Avatar';
import UserRoleWrapper from '../../../components/UserRoleWrapper';
import dateFormat from '../../../utils/dateFormat';
import { markNotificationsAsRead } from '../../../services/notifications';
import { readNotification } from '../../../state/notifications';
import { TextFieldLarge } from '../../../components/FormControls';
import { Button, TextButton } from '../../../components/Buttons';
import { MobileMediaQuery } from '../../../components/SharedStyles';
import Pagination from '../../../components/Pagination';
import { pushSmartNotification } from '../../../utils/notification';
import { isLoggedIn } from '../../../components/LoggedInOnly';
import { roleCheck } from '../../../components/UserRoleRestricted';
import { MODERATOR_ROLES } from '../../../utils/roleCodes';
import Modal from '../../../components/Modals/Modal';

const StyledUserProfileOverview = styled.div`
  .overview-header {
    display: flex;
    justify-content: space-between;
    margin: 20px 0;
    margin-top: 40px;
  }

  .overview-header-title {
    font-size: ${ThemeFontSizeLarge};
  }

  .overview-header-link {
    font-size: ${ThemeFontSizeMedium};
    opacity: 60%;
  }

  .comment-input {
    display: flex;
    flex-direction: column;
  }

  .comment-submit {
    align-self: flex-end;
    margin-top: -25px;
  }

  .comment {
    display: flex;
    margin-bottom: 25px;

    &:hover {
      .comment-delete {
        opacity: 60%;
      }
    }
  }

  .comment-avatar-container {
    margin-right: 12px;
    height: 75px;
    width: 75px;
  }

  .comment-avatar {
    width: 100%;
    height: 100%;
    object-fit: contain;
    max-height: unset;
  }

  .comment-body {
    font-size: ${ThemeFontSizeLarge};
    flex-grow: 1;
  }

  .comment-delete {
    opacity: 0;
    align-self: flex-start;
    font-size: 20px;
    transition: 0.2s;

    ${MobileMediaQuery} {
      opacity: 60%;
    }
  }

  .comment-header {
    margin-bottom: 7px;
  }

  .comment-author {
    font-weight: bold;
    margin-right: 6px;
  }

  .comment-date {
    opacity: 60%;
  }
`;

dayjs.extend(relativeTime);

const COMMENTS_PER_PAGE = 10;

const UserProfileOverview = ({
  match,
  posts,
  setPosts,
  threads,
  setThreads,
  showRatings,
  user,
  commentsDisabled,
}) => {
  const notifications = useSelector((state) => state.notifications.notifications);
  const [comments, setComments] = useState([]);
  const [totalComments, setTotalComments] = useState(0);
  const [page, setPage] = useState(1);
  const [newComment, setNewComment] = useState('');
  const [deletingComment, setDeletingComment] = useState(null);
  const currentUser = useSelector((state) => state.user);
  const { nsfwFilter, ratingsXray: ratingsXrayEnabled } = useSelector((state) => state.settings);
  const dispatch = useDispatch();
  const ratingsXray = isLoggedIn() && ratingsXrayEnabled;

  const canDeleteComment = (comment) =>
    currentUser.id === Number(match.params.id) ||
    roleCheck(MODERATOR_ROLES) ||
    currentUser.id === comment.author.id;

  const fetchData = async () => {
    const userPosts = getUserPosts(match.params.id, 1, nsfwFilter);
    const userThreads = getUserThreads(match.params.id, 1, nsfwFilter);
    setPosts(await userPosts);
    setThreads(await userThreads);
  };

  const fetchComments = async () => {
    const userComments = getUserProfileComments(match.params.id, page);
    setComments((await userComments).comments);
    setTotalComments((await userComments).totalComments);
  };

  useEffect(() => {
    fetchData();
  }, [match.params.id]);

  useEffect(() => {
    fetchComments();
  }, [match.params.id, page]);

  useEffect(() => {
    const markComments = async (readComments) => {
      try {
        await markNotificationsAsRead(
          readComments.map((id) => notifications[`PROFILE_COMMENT:${id}`].id)
        );
        readComments.forEach((commentId) => {
          dispatch(readNotification(`PROFILE_COMMENT:${commentId}`));
        });
      } catch (e) {
        console.error(e);
      }
    };

    const readComments = [];
    comments.forEach((comment) => {
      if (
        `PROFILE_COMMENT:${comment.id}` in notifications &&
        !notifications[`PROFILE_COMMENT:${comment.id}`].read
      ) {
        readComments.push(comment.id);
      }
    });
    if (readComments.length > 0) {
      markComments(readComments);
    }
  }, [notifications, comments]);

  const createComment = async () => {
    try {
      const comment = await createUserProfileComment(match.params.id, newComment);
      setNewComment('');
      setComments([comment, ...comments]);
    } catch (error) {
      pushSmartNotification({ error: 'Could not create profile comment.' });
    }
  };

  const deleteComment = async (commentId) => {
    await deleteUserProfileComment(match.params.id, commentId);
    setComments(comments.filter((comment) => comment.id !== commentId));
  };

  return (
    <>
      <StyledUserProfileOverview>
        {posts.posts?.length > 0 && (
          <div className="overview-header">
            <span className="overview-header-title">Latest Posts</span>
            <Link className="overview-header-link" to={`${match.url}/posts`}>
              See all
            </Link>
          </div>
        )}
        {posts.posts?.slice(0, 2).map((post) => (
          <Post
            key={post.id}
            hideUserWrapper
            hideControls
            byCurrentUser
            ratings={showRatings ? post.ratings : []}
            threadId={post.thread.id || post.thread}
            threadInfo={post.thread.id && post.thread}
            mentionUsers={post.mentionUsers}
            postId={post.id}
            postBody={post.content}
            postDate={post.createdAt}
            postEdited={post.updatedAt}
            postPage={post.page}
            threadPage={1}
            user={user}
            profileView
            ratingsXray={ratingsXray}
          />
        ))}
        {threads.threads?.length > 0 && (
          <div className="overview-header">
            <span className="overview-header-title">Latest Threads</span>
            <Link className="overview-header-link" to={`${match.url}/threads`}>
              See all
            </Link>
          </div>
        )}
        {threads.threads?.slice(0, 3).map((thread) => (
          <ThreadItem key={thread.id} thread={thread} minimal />
        ))}
        <div className="overview-header">
          <span className="overview-header-title">Comments</span>
          <Pagination
            useButtons
            pageSize={COMMENTS_PER_PAGE}
            pageChangeFn={setPage}
            totalPosts={totalComments}
            currentPage={page}
            jumpToTop={false}
          />
        </div>
        {currentUser.loggedIn && !commentsDisabled && (
          <div className="comment-input">
            <TextFieldLarge
              value={newComment}
              placeholder="Add a comment"
              height={80}
              fontSize="large"
              onChange={(e) => setNewComment(e.target.value)}
            />
            {newComment.length > 0 && (
              <Button onClick={createComment} className="comment-submit">
                Submit
              </Button>
            )}
          </div>
        )}
        {comments.map((comment) => (
          <div key={comment.id} id={`c-${comment.id}`} className="comment">
            <Link to={`/user/${comment.author?.id}`}>
              <div className="comment-avatar-container">
                <UserAvatar src={comment?.author?.avatarUrl} className="comment-avatar" />
              </div>
            </Link>
            <div className="comment-body">
              <div className="comment-header">
                <Link to={`/user/${comment.author?.id}`}>
                  <UserRoleWrapper user={comment.author} className="comment-author">
                    {comment.author?.username}
                  </UserRoleWrapper>
                </Link>
                <span className="comment-date" title={dateFormat(comment.createdAt)}>
                  {dayjs(comment.createdAt).fromNow()}
                </span>
              </div>
              <div className="comment-text">{comment.content}</div>
            </div>
            {canDeleteComment(comment) && (
              <TextButton
                onClick={() => setDeletingComment(comment.id)}
                title="Delete comment"
                className="comment-delete"
              >
                <i className="fa-solid fa-trash-alt" />
              </TextButton>
            )}
          </div>
        ))}
      </StyledUserProfileOverview>
      {deletingComment && (
        <Modal
          iconUrl="/static/icons/siren.png"
          title="Delete comment?"
          cancelFn={() => setDeletingComment(null)}
          submitFn={() => {
            deleteComment(deletingComment);
            setDeletingComment(null);
          }}
          submitText="Delete"
          isOpen={deletingComment}
        >
          This action is irreversible!
        </Modal>
      )}
    </>
  );
};

UserProfileOverview.propTypes = {
  match: PropTypes.shape({
    params: PropTypes.shape({
      id: PropTypes.number.isRequired,
    }).isRequired,
    url: PropTypes.string.isRequired,
  }).isRequired,
  posts: PropTypes.shape({
    // eslint-disable-next-line react/forbid-prop-types
    posts: PropTypes.array.isRequired,
  }).isRequired,
  setPosts: PropTypes.func.isRequired,
  threads: PropTypes.shape({
    // eslint-disable-next-line react/forbid-prop-types
    threads: PropTypes.array.isRequired,
  }).isRequired,
  setThreads: PropTypes.func.isRequired,
  showRatings: PropTypes.bool.isRequired,
  user: PropTypes.shape({}).isRequired,
  commentsDisabled: PropTypes.bool.isRequired,
};
export default UserProfileOverview;
