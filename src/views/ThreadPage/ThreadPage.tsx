/* eslint-disable react/forbid-prop-types */
import React, { useState, useRef, ElementRef, useEffect, useLayoutEffect } from 'react';
import { useHistory } from 'react-router-dom';
import { Helmet } from 'react-helmet';
import styled from 'styled-components';
import { Post as PostSchema, Rule, ThreadWithPosts } from 'knockout-schema';
import ErrorBoundary from '../../components/ErrorBoundary';
import LoggedInOnly from '../../components/LoggedInOnly';
import Post from '../../components/Post';
import ThreadSubheader from './components/ThreadSubheader';
import EditableTitle from './components/EditableTitle';
import {
  ThemeVerticalPadding,
  ThemeHorizontalPadding,
  ThemeTextColor,
  ThemeFontSizeHeadline,
} from '../../utils/ThemeNew';
import Modal from '../../components/Modals/Modal';
import ModalSelect from '../../components/Modals/ModalSelect';
import TagModal from './components/TagModal';
import ReportModal from '../../components/ReportModal';
import PostEditor from '../../components/PostEditor';
import { roleCheck, UserRoleRestricted } from '../../components/UserRoleRestricted';
import { StyledForumIcon } from '../../components/ForumIcon';
import { StyledPlaceholder } from '../../components/Placeholder';
import ViewerCount from './components/ViewerCount';
import ThreadViewerModal from './components/ThreadViewerModal';
import { isLoggedIn } from '../../utils/user';
import { MODERATOR_ROLES } from '../../utils/roleCodes';
import { isDeletedPost } from '../../utils/deletedUser';
import DeletedPost from './components/DeletedPost';
import { getSiteRules, getSubforumRules } from '../../services/rules';
import { Button } from '../../components/Buttons';
import { TabletMediaQuery } from '../../components/SharedStyles';
import { scrollIntoView } from '../../utils/pageScroll';
import { hideThread } from '../../services/hiddenThreads';
import { pushNotification } from '../../utils/notification';
import HideThreadModal from '../../components/HideThreadModal';
import { useAppSelector } from '../../state/hooks';

const StyledThreadPage = styled.div`
  .thread-title {
    margin: calc(${ThemeVerticalPadding} / 2) ${ThemeHorizontalPadding};
    color: ${ThemeTextColor};
    font-size: ${ThemeFontSizeHeadline};
    line-height: 1.1;
    overflow-wrap: break-word;
    vertical-align: middle;

    ${StyledForumIcon} {
      margin-right: 5px;
      max-height: 35px;
      vertical-align: middle;
    }

    ${StyledPlaceholder} {
      margin-right: 7px;
    }
  }

  .thread-page-wrapper {
    display: block;
    margin-top: calc(${ThemeVerticalPadding} / 2);
    padding-top: 0;
    padding-bottom: ${ThemeVerticalPadding};
    padding-left: ${ThemeHorizontalPadding};
    padding-right: ${ThemeHorizontalPadding};
    position: relative;
  }

  .thread-post-list {
    padding: 0;
    margin-top: calc(${ThemeVerticalPadding} / 2);
    margin-bottom: 0;
    margin-left: 0;
    margin-right: 0;
  }

  .new-posts-button {
    width: 100%;
    font-weight: 600;
    margin-bottom: calc(${ThemeVerticalPadding} / 2);
  }

  .new-posts-alert {
    position: fixed;
    bottom: 30px;
    z-index: 100;
    left: 50%;
    transform: translateX(-50%);
    font-weight: 600;
    ${TabletMediaQuery} {
      bottom: 55px;
      transform: scale(0.9) translateX(-50%);
      padding: calc(${ThemeHorizontalPadding} * 1.3) calc(${ThemeVerticalPadding} * 1.8);
    }
  }

  .alert-arrow {
    padding-right: 10px;
  }
`;

interface ThreadPageProps {
  thread: ThreadWithPosts;
  params: {
    id: string;
  };
  showingMoveModal: boolean;
  moveModalOptions: { text: string; value: number }[];
  currentPage: number;
  togglePinned: () => Promise<void>;
  toggleLocked: () => Promise<void>;
  toggleDeleted: () => Promise<void>;
  showMoveModal: () => Promise<void>;
  createAlert: () => Promise<void>;
  deleteAlert: () => Promise<void>;
  currentUserId: number | null;
  submitThreadMove: (threadId: number) => void;
  hideModal: () => void;
  refreshPosts: () => void;
  isSubscribed: boolean;
  linkedPostId: number | undefined;
  newPostQueue: PostSchema[];
  addNewPosts: () => void;
  markThreadRead: () => void;
  stickyPostLink: boolean;
}

const ThreadPage = ({
  thread,
  params,
  showingMoveModal,
  moveModalOptions,
  currentPage,
  togglePinned,
  toggleLocked,
  toggleDeleted,
  showMoveModal,
  deleteAlert,
  createAlert,
  currentUserId,
  submitThreadMove,
  hideModal,
  refreshPosts,
  linkedPostId,
  newPostQueue,
  isSubscribed,
  addNewPosts,
  markThreadRead,
  stickyPostLink,
}: ThreadPageProps) => {
  const { hideRatings, ratingsXray } = useAppSelector((state) => state.settings);
  const [modalMove, setModalMove] = useState<number>();
  const [reportModalOpen, setReportModalOpen] = useState(false);
  const [rules, setRules] = useState<Rule[]>([]);
  const [rulesLoaded, setRulesLoaded] = useState(false);
  const [viewerModalOpen, setViewerModalOpen] = useState(false);
  const [reportPostId, setReportPostId] = useState(undefined);
  type PostEditorHandle = ElementRef<typeof PostEditor>;
  const editorRef = useRef<PostEditorHandle>(null);
  const [tagModal, showTagModal] = useState(false);
  const [showNewPostIndicator, setShowNewPostIndicator] = useState(newPostQueue.length > 0);
  const showNewPostEditor = roleCheck(MODERATOR_ROLES) || (!thread.locked && !thread.deleted);
  // TODO: Pass empty enabled ratings array if subform has ratings disabled, remove subforumId check
  const showRatings = !hideRatings && thread.subforumId !== 18;
  const newPostsButton = useRef<HTMLButtonElement>(null);
  const history = useHistory();
  const [hideThreadModalOpen, setHideThreadModalOpen] = useState(false);
  const showReportModal = !roleCheck(MODERATOR_ROLES);

  const handleHideThreadClick = () => {
    setHideThreadModalOpen(true);
  };

  const handleHideThreadModalSubmit = async () => {
    try {
      await hideThread(thread.id);
    } catch (err) {
      pushNotification({ message: 'Could not hide thread.', type: 'error' });
    }
    history.push(`/subforum/${thread.subforumId}`);
  };

  useEffect(() => {
    const fetchRules = async () => {
      try {
        const siteRules = await getSiteRules();
        const subforumRules = await getSubforumRules(thread.subforumId);
        const allRules = siteRules.concat(subforumRules);
        setRules(allRules);
      } catch (error) {
        console.error(error);
      }
    };

    if (reportModalOpen && !rulesLoaded) {
      fetchRules();
      setRulesLoaded(true);
    }
  }, [thread.subforumId, reportModalOpen, rulesLoaded]);

  const handleReportClick = (postId) => {
    setReportPostId(postId);
    setReportModalOpen(true);
  };

  useLayoutEffect(() => {
    const handleScroll = () => {
      if (newPostQueue.length > 0) {
        const { top, bottom } = newPostsButton.current!.getBoundingClientRect();
        setShowNewPostIndicator(top >= window.innerHeight || bottom <= 0);
      }
    };

    // Add scroll listener on mount
    window.addEventListener('scroll', handleScroll, { capture: true, passive: true });

    // Remove scroll listener on unmount
    return () => {
      window.removeEventListener('scroll', handleScroll, { capture: true });
    };
  }, [newPostQueue]);

  return (
    <>
      <Modal
        iconUrl="/static/icons/rearrange.png"
        title="Move thread"
        cancelFn={hideModal}
        submitFn={() => submitThreadMove(modalMove!)}
        isOpen={showingMoveModal}
      >
        <ModalSelect
          defaultText="Choose a subforum..."
          options={moveModalOptions}
          onChange={(e) => setModalMove(e.target.value)}
        />
      </Modal>
      <TagModal thread={thread} isOpen={tagModal} openFn={showTagModal} />
      {showReportModal && (
        <ReportModal
          postId={reportPostId}
          isOpen={reportModalOpen}
          close={() => setReportModalOpen(false)}
          rules={rules}
        />
      )}
      <HideThreadModal
        modalOpen={hideThreadModalOpen}
        setModalOpen={setHideThreadModalOpen}
        threadTitle={thread.title}
        hideThread={handleHideThreadModalSubmit}
      />
      <StyledThreadPage>
        <Helmet defer={false}>
          <title>
            {thread.title ? `${thread.title} - Knockout!` : 'Knockout - Loading thread...'}
          </title>
        </Helmet>
        <h1 className="thread-title">
          <EditableTitle
            title={thread.title}
            byCurrentUser={currentUserId === thread.user?.id}
            threadId={thread.id}
            iconId={thread.iconId}
          />
        </h1>
        {thread.viewers && (
          <ViewerCount
            id={thread.id}
            viewers={thread.viewers}
            onClick={() => thread.viewers.users && setViewerModalOpen(true)}
          />
        )}
        {(thread.viewers?.users?.length ?? 0) > 0 && (
          <UserRoleRestricted roleCodes={MODERATOR_ROLES}>
            <ThreadViewerModal
              viewers={thread.viewers?.users!}
              modalOpen={viewerModalOpen}
              setModalOpen={setViewerModalOpen}
            />
          </UserRoleRestricted>
        )}
        <ThreadSubheader
          thread={thread}
          params={params}
          currentPage={currentPage}
          togglePinned={togglePinned}
          toggleDeleted={toggleDeleted}
          toggleLocked={toggleLocked}
          currentUserId={currentUserId}
          showMoveModal={showMoveModal}
          showTagModal={showTagModal}
          createAlert={createAlert}
          isSubscribed={isSubscribed}
          deleteAlert={deleteAlert}
          hideAction={handleHideThreadClick}
          markThreadRead={markThreadRead}
          stickyPostLink={stickyPostLink}
        />
        <article className="thread-page-wrapper">
          <div className="thread-post-list">
            {thread.posts.map((post) => {
              if (isDeletedPost(post)) {
                return (
                  <DeletedPost
                    postThreadPostNumber={post.threadPostNumber}
                    postDate={post.createdAt}
                    postId={post.id}
                  />
                );
              }

              const isUnread =
                (thread.readThread?.lastPostNumber ?? post.threadPostNumber) <
                post.threadPostNumber;
              const isLinkedPost =
                linkedPostId !== undefined && Number(post.id) === Number(linkedPostId);
              const byCurrentUser = currentUserId === post.user!.id;

              return (
                <Post
                  key={`${post.id}-${post.updatedAt}`}
                  threadPage={currentPage}
                  threadId={thread.id}
                  threadLocked={thread.locked}
                  postId={post.id}
                  postBody={post.content}
                  postDate={post.createdAt}
                  postEdited={post.updatedAt}
                  postThreadPostNumber={post.threadPostNumber}
                  postPage={currentPage}
                  ratings={post.ratings}
                  user={post.user}
                  byCurrentUser={byCurrentUser}
                  bans={post.bans}
                  isUnread={isUnread}
                  countryName={post.countryName}
                  countryCode={post.countryCode}
                  isLinkedPost={isLinkedPost}
                  subforumId={thread.subforumId}
                  handleReplyClick={(text) => {
                    if (editorRef.current) editorRef.current.appendToContent(text);
                  }}
                  handleReportClick={handleReportClick}
                  postSubmitFn={refreshPosts}
                  canRate={Boolean(currentUserId)}
                  ratingsXray={isLoggedIn() && ratingsXray}
                  showRatings={showRatings}
                  responses={post.responses}
                  lastEditedUser={(post as any).lastEditedUser}
                  mentionUsers={post.mentionUsers}
                />
              );
            })}
          </div>
          {showNewPostIndicator && (
            <Button className="new-posts-alert" onClick={() => scrollIntoView('.new-posts-button')}>
              <i className="fa-sharp fa-solid fa-arrow-down alert-arrow" />
              New posts
            </Button>
          )}
          {newPostQueue.length > 0 && (
            <Button className="new-posts-button" onClick={addNewPosts} ref={newPostsButton}>
              {`${newPostQueue.length} new ${newPostQueue.length === 1 ? 'post' : 'posts'}`}
            </Button>
          )}
          {showNewPostEditor && (
            <LoggedInOnly>
              <ErrorBoundary errorMessage="The editor has crashed. Your post was saved a few seconds ago (hopefully). Reload the page.">
                <div className="thread-new-post">
                  <PostEditor
                    threadId={Number(params.id)}
                    ref={editorRef}
                    postSubmitFn={refreshPosts}
                  />
                </div>
              </ErrorBoundary>
            </LoggedInOnly>
          )}
        </article>
        <ThreadSubheader
          thread={thread}
          params={params}
          currentPage={currentPage}
          togglePinned={togglePinned}
          toggleDeleted={toggleDeleted}
          toggleLocked={toggleLocked}
          currentUserId={currentUserId}
          showMoveModal={showMoveModal}
          showTagModal={showTagModal}
          createAlert={createAlert}
          deleteAlert={deleteAlert}
          isSubscribed={isSubscribed}
          hideAction={handleHideThreadClick}
        />
      </StyledThreadPage>
    </>
  );
};

export default ThreadPage;
