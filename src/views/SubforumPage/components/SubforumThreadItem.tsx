import React, { useState } from 'react';
import { ThreadWithLastPost } from 'knockout-schema';
import ThreadItem from '../../../components/ThreadItem';

import { deleteReadThread } from '../../../services/readThreads';
import { pushNotification } from '../../../utils/notification';
import { removeSubscriptionThread } from '../../../state/subscriptions';
import { useAppDispatch } from '../../../state/hooks';

interface SubforumThreadItemProps {
  thread: ThreadWithLastPost;
  showTopRating: boolean;
  hideAction: () => void;
}

const SubforumThreadItem = ({ thread, showTopRating, hideAction }: SubforumThreadItemProps) => {
  const [isRead, setIsRead] = useState(thread.readThread !== undefined);
  const threadOpacity = isRead && thread.readThread?.unreadPostCount === 0 ? '0.5' : '1.0';
  const dispatch = useAppDispatch();

  const markUnread = async () => {
    try {
      await deleteReadThread(thread.id);
      setIsRead(false);
      dispatch(removeSubscriptionThread(thread.id));
      pushNotification({ message: 'Thread marked as unread.' });
    } catch (err) {
      pushNotification({ message: 'Could not mark thread as unread.', type: 'error' });
    }
  };

  return (
    <ThreadItem
      thread={thread}
      showTopRating={showTopRating}
      markUnreadAction={markUnread}
      threadOpacity={threadOpacity}
      isRead={isRead}
      hideAction={hideAction}
    />
  );
};

export default SubforumThreadItem;
