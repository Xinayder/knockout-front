import { Settings, SettingsAction } from './actions';

const settingsToStorageName = (setting: keyof Settings) => {
  switch (setting) {
    case 'latestThreadMode':
      return 'show-latest-threads';
    case 'motdDismissed':
      return 'motd-dismissed';
    default:
      return setting;
  }
};

let defaultValues: Settings = {
  punchyLabs: false,
  latestThreadMode: true,
  autoSubscribe: true,
  markLastPageRead: false,
  ratingsXray: false,
  stickyHeader: true,
  threadAds: true,
  hideRatings: false,
  nsfwFilter: true,
  holidayTheme: true,
  displayCountryInfo: false,
  hideRiskyMedia: true,
  motdDismissed: -1,
};

const storageState = {};
for (const key of Object.keys(defaultValues)) {
  const value = localStorage.getItem(settingsToStorageName(key as keyof Settings));
  if (value !== null) {
    storageState[key] = JSON.parse(value);
  }
}

defaultValues = { ...defaultValues, ...storageState };

export default (
  state: Settings = defaultValues,
  action?: { type: SettingsAction; value: Partial<Settings> }
) => {
  switch (action?.type) {
    case SettingsAction.SETTINGS_UPDATE:
      for (const key of Object.keys(action.value)) {
        localStorage.setItem(settingsToStorageName(key as keyof Settings), action.value[key]);
      }
      return {
        ...state,
        ...action.value,
      };
    default:
      return state;
  }
};
