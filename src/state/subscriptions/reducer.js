import {
  SUBSCRIPTION_SET_STATE,
  SUBSCRIPTION_UPDATE_THREAD,
  SUBSCRIPTION_REMOVE_THREAD,
  SUBSCRIPTION_ADD_THREAD,
} from './actions';

const initialState = {
  threads: {},
  count: 0,
};

export default function (state = initialState, action) {
  switch (action.type) {
    case SUBSCRIPTION_SET_STATE:
      return action.value;
    case SUBSCRIPTION_ADD_THREAD:
      return {
        threads: {
          ...state.threads,
          [action.value.id]: {
            title: action.value.title,
            page: action.value.page,
            count: action.value.count,
            postId: action.value.post,
            postNum: action.value.postNum,
            iconId: action.value.iconId,
            locked: action.value.locked,
          },
        },
        count: state.count + action.value.count,
      };
    case SUBSCRIPTION_UPDATE_THREAD:
      if (!(action.value.id in state.threads)) {
        return state;
      }
      return {
        threads: {
          ...state.threads,
          [action.value.id]: {
            ...state.threads[action.value.id],
            page: action.value.page || state.threads[action.value.id].page,
            postNum: action.value.postNum || state.threads[action.value.id].postNum,
            count: action.value.count,
          },
        },
        count: state.count - (state.threads[action.value.id] - action.value.count),
      };
    case SUBSCRIPTION_REMOVE_THREAD: {
      if (!(action.value in state.threads)) {
        return state;
      }
      const {
        [action.value]: { count: threadCount },
        ...newThreads
      } = state.threads;
      return { threads: newThreads, count: state.count - threadCount };
    }
    default:
      break;
  }
  return state;
}
