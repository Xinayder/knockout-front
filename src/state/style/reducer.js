import { THEME_UPDATE, WIDTH_UPDATE, MOTD_UPDATE } from './actions';
import { loadThemeFromStorage, loadWidthFromStorage } from '../../services/theme';

const initialState = {
  theme: loadThemeFromStorage(),
  width: loadWidthFromStorage(),
  motd: false,
};

export default function (state = initialState, action = undefined) {
  switch (action.type) {
    case THEME_UPDATE:
      return { ...state, theme: action.value };
    case WIDTH_UPDATE:
      return { ...state, width: action.value };
    case MOTD_UPDATE:
      return { ...state, motd: action.value };
    default:
      break;
  }
  return state;
}
