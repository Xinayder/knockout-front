import { combineReducers } from 'redux';

import { userReducer } from './user';
import { backgroundReducer } from './background';
import { notificationsReducer } from './notifications';
import { subscriptionsReducer } from './subscriptions';
import { styleReducer } from './style';
import { settingsReducer } from './settings';

export default combineReducers({
  user: userReducer,
  background: backgroundReducer,
  notifications: notificationsReducer,
  subscriptions: subscriptionsReducer,
  style: styleReducer,
  settings: settingsReducer,
});
