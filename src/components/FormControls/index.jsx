import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import {
  ThemeBackgroundLighter,
  ThemeFontFamily,
  ThemeFontSizeLarge,
  ThemeFontSizeMedium,
  ThemeFontSizeSmall,
  ThemeHorizontalPadding,
  ThemeTextColor,
  ThemeVerticalPadding,
} from '../../utils/ThemeNew';

export const FieldLabel = styled.div`
  margin-bottom: 10px;
  font-size: ${ThemeFontSizeLarge};
  font-weight: bold;
`;

export const FieldLabelSmall = styled.div`
  margin-bottom: 10px;
  font-size: ${ThemeFontSizeSmall};
  opacity: 0.7;
  line-height: normal;

  a {
    color: #3facff;
  }
`;

export const TextField = styled.input`
  display: block;
  padding: ${ThemeVerticalPadding} ${ThemeHorizontalPadding};
  color: ${ThemeTextColor};
  font-size: ${(props) => (props.fontSize === 'large' ? ThemeFontSizeLarge : ThemeFontSizeMedium)};
  font-family: ${ThemeFontFamily};
  line-height: 1.1;
  margin-bottom: 20px;
  border: none;
  width: 100%;
  box-sizing: border-box;
  background: ${ThemeBackgroundLighter};
  resize: none;
`;

export const StyledTextFieldLarge = styled.div`
  position: relative;
  margin-bottom: 25px;

  ${TextField} {
    height: ${(props) => props.height}px;
  }

  .char-count {
    position: absolute;
    right: ${ThemeHorizontalPadding};
    bottom: ${ThemeVerticalPadding};
    font-size: ${ThemeFontSizeSmall};
    opacity: 0.5;
  }
`;

export const TextFieldLarge = ({ value, placeholder, onChange, maxLength, height, fontSize }) => {
  return (
    <StyledTextFieldLarge height={height}>
      {maxLength && <span className="char-count">{maxLength - value.length}</span>}
      <TextField
        as="textarea"
        value={value}
        placeholder={placeholder}
        onChange={onChange}
        maxLength={maxLength}
        fontSize={fontSize}
      />
    </StyledTextFieldLarge>
  );
};

TextFieldLarge.propTypes = {
  value: PropTypes.string.isRequired,
  placeholder: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
  maxLength: PropTypes.number.isRequired,
  height: PropTypes.number,
  fontSize: PropTypes.string,
};

TextFieldLarge.defaultProps = {
  height: 100,
  fontSize: 'medium',
};

export const StyledFieldLabelWithIcon = styled.div`
  display: flex;
  align-items: baseline;

  i {
    display: flex;
    justify-content: center;
    margin-right: ${ThemeHorizontalPadding};
    width: 16px;
    height: 16px;
  }

  i::before {
    font-size: ${ThemeFontSizeSmall};
  }

  ${FieldLabel} {
    margin-bottom: 0px;
  }
`;

export const FieldLabelWithIcon = ({ label, icon }) => {
  return (
    <StyledFieldLabelWithIcon>
      <i className={`fa-solid ${icon}`} />
      <FieldLabel>{label}</FieldLabel>
    </StyledFieldLabelWithIcon>
  );
};

FieldLabelWithIcon.propTypes = {
  label: PropTypes.string.isRequired,
  icon: PropTypes.string,
};

FieldLabelWithIcon.defaultProps = {
  icon: '',
};
