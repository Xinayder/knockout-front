import React, { useEffect, useRef } from 'react';
import styled from 'styled-components';
import { HashLink as Link } from 'react-router-hash-link';
import { darken, lighten } from 'polished';
import dayjs from 'dayjs';
import relativeTime from 'dayjs/plugin/relativeTime';
import { useDispatch, useSelector } from 'react-redux';
import useDropdownMenu, {
  DropdownMenuButton,
  DropdownMenuEmpty,
  DropdownMenuHeader,
  DropdownMenuItem,
  DropdownMenuOpened,
} from './DropdownMenu';
import {
  ThemeBackgroundLighter,
  ThemeFontSizeMedium,
  ThemeHighlightWeaker,
  ThemeHorizontalPadding,
  ThemeVerticalPadding,
  ThemeFontSizeSmall,
} from '../../../utils/ThemeNew';
import UserRoleWrapper from '../../UserRoleWrapper';
import UserAvatar from './UserAvatar';
import { TextButton } from '../../Buttons';
import Tooltip from '../../Tooltip';
import {
  markAllNotificationsAsRead,
  markNotificationAsRead,
} from '../../../services/notifications';
import {
  addNotification,
  readAllNotifications,
  readNotification,
} from '../../../state/notifications';
import getConversationUser from '../../../views/MessagesPage/getConversationUser';
import socketClient from '../../../socketClient';
import notificationsBroadcastChannel from '../../../notificationsBroadcastChannel';

const MarkAsReadButtonContainer = styled(Tooltip)`
  margin-left: auto;
  pointer-events: all;
  margin-right: -15px;

  .read-all-button {
    font-size: 18px;
  }
`;

const RepliesMenuButton = styled(DropdownMenuButton)`
  padding: 0px 10px;
  margin-right: 10px;
  margin-left: -5px;
`;

const StyledUserAvatar = styled(UserAvatar)`
  width: 45px;
  margin-right: calc(${ThemeHorizontalPadding} * 1.5);
`;

const DropdownReplyItem = styled(DropdownMenuItem)`
  padding: calc(${ThemeVerticalPadding} * 1.5) calc(${ThemeHorizontalPadding} * 2);
  ${(props) => props.read && 'opacity: 0.6;'}

  .notification-time {
    font-size: ${ThemeFontSizeSmall};
    opacity: 0.6;
    margin-top: 5px;
  }

  .dropdown-text-container {
    flex-grow: 1;
  }

  .unread-icon {
    display: inline-block;
    ${(props) => props.read && 'visibility: hidden;'}
    width: 9px;
    height: 9px;
    background: ${ThemeHighlightWeaker};
    border-radius: 50%;
    flex-shrink: 0;
  }

  .dropdown-reply-link {
    display: flex;
    align-items: center;
    flex-grow: 1;
  }

  .dropdown-reply-text {
    flex: 1;
    line-height: calc(${ThemeFontSizeMedium} * 1.2);
  }

  .dropdown-reply-dismiss {
    outline: none;
    border: none;
    background: none;
    cursor: pointer;
    color: ${(props) => {
      if (props.theme.mode === 'light') {
        return darken(0.1, ThemeBackgroundLighter(props));
      }
      return lighten(0.1, ThemeBackgroundLighter(props));
    }};
    padding: 0;
    font-size: 20px;

    &:hover {
      color: ${(props) => {
        if (props.theme.mode === 'light') {
          return darken(0.2, ThemeBackgroundLighter(props));
        }
        return lighten(0.2, ThemeBackgroundLighter(props));
      }};
    }
  }
`;

dayjs.extend(relativeTime);

const RepliesMenu = () => {
  const menuRef = useRef();
  const buttonRef = useRef();
  const dispatch = useDispatch();
  const [open, setOpen] = useDropdownMenu(menuRef, buttonRef);
  const notificationMap = useSelector((state) => state.notifications.notifications);
  const actionList = useRef([]);
  const currentUser = useSelector((state) => state.user);
  const notifications = Object.values(notificationMap);

  let unreadCount = 0;

  const notificationRenderList = notifications
    .map((notification) => {
      if (!notification.read) {
        unreadCount += 1;
      }
      switch (notification.type) {
        case 'MESSAGE': {
          return {
            user: getConversationUser(notification.data?.users, currentUser),
            link: `/messages/${notification.data?.id}`,
            defaultUrl: 'https://img.icons8.com/color/48/000000/speech-bubble.png',
            createdAt: notification.createdAt,
            read: notification.read,
            description: ' sent you a message.',
          };
        }
        case 'POST_MENTION': {
          return {
            user: notification.data?.user,
            link: `/thread/${notification.data?.thread?.id}/${notification.data?.page}#post-${notification.data?.id}`,
            defaultUrl: 'https://img.icons8.com/color/96/000000/email.png',
            createdAt: notification.createdAt,
            read: notification.read,
            description: ' mentioned you in ',
            title: notification.data?.thread?.title,
          };
        }
        case 'POST_REPLY': {
          return {
            user: notification.data?.user,
            link: `/thread/${notification.data?.thread?.id}/${notification.data?.page}#post-${notification.data?.id}`,
            defaultUrl: 'https://img.icons8.com/color/96/000000/chat.png',
            createdAt: notification.createdAt,
            read: notification.read,
            description: ' replied to your post in ',
            title: notification.data?.thread?.title,
          };
        }
        case 'PROFILE_COMMENT': {
          return {
            user: notification.data?.author,
            link: `/user/${notification.data?.userProfile}#c-${notification.data?.id}`,
            defaultUrl: 'https://img.icons8.com/color/96/000000/filled-chat.png',
            createdAt: notification.createdAt,
            read: notification.read,
            description: ' commented on your profile.',
          };
        }
        case 'REPORT_RESOLUTION': {
          return {
            defaultUrl: 'https://img.icons8.com/color/96/000000/protect.png',
            createdAt: notification.createdAt,
            read: notification.read,
            description: 'Action has been taken on a post you reported.',
            id: notification.id,
            dataType: notification.type,
            dataId: notification.data?.id,
          };
        }
        default:
          return {};
      }
    })
    .sort((a, b) => b.createdAt.localeCompare(a.createdAt));

  const handleNotifClick = async (event, notification) => {
    // If a notification has an attached link, or is read, we don't need to handle the click
    if (notification.link || notification.read) return;

    event.preventDefault();
    try {
      await markNotificationAsRead(notification.id);
      dispatch(readNotification(`${notification.dataType}:${notification.dataId}`));
    } catch (e) {
      // eslint-disable-next-line no-console
      console.error(e);
    }
  };

  useEffect(() => {
    notificationsBroadcastChannel.onmessage = (event) => {
      if (open) {
        actionList.current.push(event.data);
      } else {
        dispatch(event.data);
      }
    };
    return () => {
      notificationsBroadcastChannel.onmessage = null;
    };
  }, [dispatch, open]);

  useEffect(() => {
    if (!open && actionList.current.length > 0) {
      actionList.current.forEach((action) => {
        dispatch(action);
      });
      actionList.current = [];
    }
  }, [dispatch, open]);

  useEffect(() => {
    socketClient.emit('notifications:join');
    socketClient.on('notification:new', (notification) => {
      dispatch(addNotification(notification));
    });
    return () => {
      socketClient.off('notification:new');
      socketClient.emit('notifications:leave');
    };
  }, [dispatch]);

  return (
    <RepliesMenuButton
      className="replies-menu"
      onClick={(e) => {
        if (!e.target.closest('.dropdown-action')) {
          setOpen((value) => !value);
        }
      }}
      onKeyDown={(e) => {
        if (e.key === 'Enter') {
          setOpen((value) => !value);
        }
      }}
      tabIndex="0"
      ref={buttonRef}
    >
      <div className="dropdown-menu-button-inner" title="Messages">
        <i className="fa-solid fa-comment menu-icon" />
        {unreadCount > 0 && (
          <div className="link-notification">{unreadCount < 10 ? unreadCount : '9+'}</div>
        )}
      </div>
      {open && (
        <DropdownMenuOpened className="subscriptions-menu-dropdown" ref={menuRef}>
          <DropdownMenuHeader>
            <h1 className="dropdown-menu-header-text">Messages</h1>
            {notifications.length > 0 && (
              <MarkAsReadButtonContainer top={false} left text="Mark all as read">
                <TextButton
                  className="read-all-button dropdown-action"
                  onClick={() => {
                    markAllNotificationsAsRead();
                    dispatch(readAllNotifications());
                  }}
                >
                  <i className="fa-solid fa-check-double" />
                </TextButton>
              </MarkAsReadButtonContainer>
            )}
          </DropdownMenuHeader>
          <div className="dropdown-menu-body">
            {notificationRenderList.map((notification) => (
              <DropdownReplyItem key={notification.id} read={notification.read}>
                <Link
                  className="dropdown-reply-link"
                  to={notification.link || '#'}
                  onClick={(e) => handleNotifClick(e, notification)}
                >
                  <StyledUserAvatar user={notification.user} defaultUrl={notification.defaultUrl} />
                  <div className="dropdown-text-container">
                    <div className="dropdown-reply-text">
                      {notification.user && (
                        <b>
                          <UserRoleWrapper user={notification.user}>
                            {notification.user?.username}
                          </UserRoleWrapper>
                        </b>
                      )}
                      {notification.description}
                      {notification.title && <b>{`${notification.title}.`}</b>}
                    </div>
                    <div className="notification-time">
                      {dayjs(notification.createdAt).fromNow()}
                    </div>
                  </div>
                  <div className="unread-icon" title="Unread" />
                </Link>
              </DropdownReplyItem>
            ))}
            {notifications.length === 0 && (
              <DropdownMenuEmpty>
                <i className="fa-solid fa-comment-dots dropdown-menu-empty-icon" />
                <h2 className="dropdown-menu-empty-header">No new messages</h2>
                <div className="dropdown-menu-empty-desc">
                  Messages from users and post replies will appear here.
                </div>
              </DropdownMenuEmpty>
            )}
          </div>

          <Link
            to="/messages"
            className="dropdown-menu-footer"
            onKeyDown={(e) => {
              if (e.key === 'Enter') {
                window.location.href = '/messages';
              }
            }}
          >
            View messages
          </Link>
        </DropdownMenuOpened>
      )}
    </RepliesMenuButton>
  );
};

export default RepliesMenu;
