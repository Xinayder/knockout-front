import React, { useRef, useEffect } from 'react';
import { useLocation } from 'react-router-dom';
import styled from 'styled-components';
import { transparentize } from 'polished';
import {
  ThemeHorizontalPadding,
  ThemeVerticalPadding,
  ThemeFontSizeSmall,
  ThemeFontSizeMedium,
  ThemeFontSizeLarge,
  ThemeBackgroundDarker,
  ThemeBackgroundLighter,
  ThemeHighlightWeaker,
  ThemeKnockoutRed,
} from '../../../utils/ThemeNew';
import { Button } from '../../Buttons';

const numberBoxSize = '35px';

interface StyledRuleCardProps {
  cardinality: number;
}

const StyledRuleCard = styled.li<StyledRuleCardProps>`
  @keyframes copiedTextPop {
    0% {
      opacity: 0;
    }
    30% {
      opacity: 1;
    }
    80% {
      opacity: 1;
    }
    100% {
      opacity: 0;
    }
  }
  &.rule-card {
    display: list-item;
    position: relative;
    z-index: 3;
    box-sizing: border-box;
    width: 49%;
    max-width: 49%;
    padding: calc(${ThemeHorizontalPadding} * 2);
    padding-left: calc(${numberBoxSize} + calc(${ThemeHorizontalPadding} * 2));
    margin-bottom: calc(${ThemeVerticalPadding} * 2);
    background-color: ${ThemeBackgroundDarker};
    counter-increment: counter;
    cursor: pointer;
    transition: filter 200ms ease-in-out, transform 50ms;
    overflow: hidden;
    .top-row {
      display: flex;
      justify-content: flex-end;
      align-items: center;
      padding: 0;

      h3 {
        margin-right: auto;
      }

      .edit-btn {
        font-size: ${ThemeFontSizeSmall};
        padding: ${ThemeVerticalPadding} ${ThemeHorizontalPadding};
        margin-right: ${ThemeHorizontalPadding};
      }

      .destroy-btn {
        font-size: ${ThemeFontSizeSmall};
        padding: ${ThemeVerticalPadding} ${ThemeHorizontalPadding};
        background-color: ${ThemeKnockoutRed};
      }
    }
    .copied-text {
      box-sizing: border-box;
      position: absolute;
      z-index: 1;
      bottom: 0;
      left: 0;
      opacity: 0;
      padding: 10px;
      background-color: ${ThemeBackgroundDarker};
      font-size: ${ThemeFontSizeSmall};
    }
    &:before {
      content: '${(props) => props.cardinality + 1}';
      position: absolute;
      top: 0;
      left: 0;
      bottom: 0;
      width: ${numberBoxSize};
      height: ${numberBoxSize};
      line-height: ${numberBoxSize};
      text-align: center;
      align-self: center;
      font-size: ${ThemeFontSizeLarge};
      background-color: ${(props) => transparentize(0.3, ThemeHighlightWeaker(props))};
    }
    &.selected {
      background-color: ${ThemeBackgroundLighter};
      filter: drop-shadow(0px 0px 3px ${ThemeHighlightWeaker});
    }
    h3 {
      letter-spacing: 0.1em;
      margin-top: 0;
      padding: 0;
      position: relative;
      display: inline-block;
      text-transform: capitalize;
      font-size: ${ThemeFontSizeLarge};
      &:after {
        content: ' ';
        position: absolute;
        bottom: -10px;
        left: 0;
        right: 50%;
        height: 2px;
        background-color: ${ThemeHighlightWeaker};
        transition: right 200ms ease-in-out;
      }
    }
    p {
      font-size: ${ThemeFontSizeMedium};
      margin-bottom: 0;
      line-height: 1.5em;
    }
    a {
      color: ${ThemeHighlightWeaker};
      font-weight: bold;
      &:hover {
        filter: brightness(150%);
      }
    }
    &:focus {
      filter: drop-shadow(0px 0px 3px ${ThemeHighlightWeaker});
      outline: none;
      .copied-text {
        z-index: 1;
        animation-name: copiedTextPop;
        animation-duration: 2s;
      }
    }
    &:active {
      transform: scale(0.98);
    }
    &:hover {
      h3::after {
        right: 0;
      }
    }
  }
  @media (max-width: 700px) {
    &.rule-card {
      width: 100%;
      max-width: 100%;
    }
  }
`;

interface RuleCardProps {
  title: string;
  cardinality: number;
  children: React.ReactNode;
  identifier: string;
  selected: boolean;
  interactable: boolean;
  canEdit: boolean;
  onEditClick?: () => void;
  onDestroyClick?: () => void;
}

const RuleCard = ({
  title,
  cardinality,
  children,
  identifier,
  selected = false,
  interactable = true,
  canEdit = false,
  onEditClick = () => {},
  onDestroyClick = () => {},
}: RuleCardProps) => {
  const path = `${window.location.origin}${useLocation().pathname}`;
  const cardRef = useRef<HTMLLIElement>(null);

  const onClick = () => {
    navigator.clipboard.writeText(`${path}?id=${identifier}`);
  };

  useEffect(() => {
    if (selected && interactable) {
      cardRef.current?.scrollIntoView({
        behavior: 'smooth',
        block: 'center',
        inline: 'nearest',
      });
    }
  });

  return (
    <StyledRuleCard
      id={identifier}
      className={`rule-card ${selected && interactable ? 'selected' : ''}`}
      onClick={interactable ? onClick : () => {}}
      cardinality={cardinality}
      ref={cardRef}
      tabIndex={-1}
    >
      <div className="top-row">
        <h3>{title}</h3>
        {canEdit && (
          <>
            <Button className="edit-btn" onClick={onEditClick} aria-label="Edit Rule">
              <i className="fa-solid fa-pencil-alt" />
            </Button>
            <Button className="destroy-btn" onClick={onDestroyClick} aria-label="Destroy Rule">
              <i className="fa-solid fa-trash-alt" />
            </Button>
          </>
        )}
      </div>
      <p>{children}</p>
      {interactable && (
        <p className="copied-text" aria-hidden>
          Copied To Clipboard
        </p>
      )}
    </StyledRuleCard>
  );
};

export default RuleCard;
