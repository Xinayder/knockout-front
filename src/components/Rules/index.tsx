import React, { useEffect, useState } from 'react';
import styled from 'styled-components';
import { useLocation } from 'react-router-dom';
import { Helmet } from 'react-helmet';
import { Rule } from 'knockout-schema';
import RuleCard from './components/RuleCard';
import Modal from '../Modals/Modal';
import { MODERATOR_ROLES } from '../../utils/roleCodes';
import { useAppSelector } from '../../state/hooks';
import { updateRule, deleteRule } from '../../services/rules';
import { pushSmartNotification } from '../../utils/notification';
import EditRuleModal from './components/EditRuleModal';

const StyledRules = styled.div`
  .rules-list {
    box-sizing: border-box;
    display: flex;
    justify-content: space-between;
    flex-wrap: wrap;
    width: 100%;
    padding: 0;
    list-style: none;
  }

  a {
    text-decoration: underline;
  }
`;

interface RulesProps {
  getRules: () => Promise<Rule[]>;
  interactable?: boolean;
  resource?: string;
}

const Rules = ({ getRules, interactable = true, resource = '' }: RulesProps) => {
  const selectedId = new URLSearchParams(useLocation().search).get('id');
  const currentUser = useAppSelector((state) => state.user);

  const canEditRules = MODERATOR_ROLES.includes(currentUser?.role?.code);

  const [rulesLoading, setRulesLoading] = useState(true);
  const [rules, setRules] = useState<Rule[]>([]);
  const [editModalOpen, setEditModalOpen] = useState(false);
  const [destroyModalOpen, setDestroyModalOpen] = useState(false);
  const [selectedRule, setSelectedRule] = useState<Rule | null>(null);

  useEffect(() => {
    setRulesLoading(true);
    const fetchRules = async () => {
      try {
        setRules(await getRules());
      } catch (error) {
        console.error(error);
      } finally {
        setRulesLoading(false);
      }
    };
    fetchRules();
  }, [getRules]);

  const handleEditModalOpen = (rule: Rule) => {
    setSelectedRule(rule);
    setEditModalOpen(true);
  };

  const handleDestroyModalOpen = (rule: Rule) => {
    setSelectedRule(rule);
    setDestroyModalOpen(true);
  };

  const handleEditModalClose = () => {
    setEditModalOpen(false);
    setSelectedRule(null);
  };

  const handleDestroyModalClose = () => {
    setDestroyModalOpen(false);
    setSelectedRule(null);
  };

  const handleEditRule = async (category: string, title: string, description: string) => {
    try {
      await updateRule(selectedRule!.id, category, title, description);
      setRules(await getRules());
    } catch (error) {
      console.error(error);
    } finally {
      pushSmartNotification({ message: 'Rule successfully edited!' });
      handleEditModalClose();
    }
  };

  const handleDestroyRule = async () => {
    try {
      await deleteRule(selectedRule!.id);
      setRules(await getRules());
    } catch (error) {
      console.error(error);
    } finally {
      pushSmartNotification({ message: 'Rule successfully deleted!' });
      handleDestroyModalClose();
    }
  };

  let rulesContent;

  if (rulesLoading) {
    rulesContent = <p>Loading the latest rules...</p>;
  } else if (rules.length === 0) {
    rulesContent = (
      <p>
        No {`${resource !== '' && resource}`} rules found. Please visit{' '}
        <a href="/rules" title="Knockout Rules">
          the Knockout Rules page
        </a>{' '}
        for general site rules.
      </p>
    );
  } else {
    rulesContent = (
      <ol className="rules-list">
        {rules.map((rule, i) => {
          const identifier = `d-${i + 1}`;
          const selected = identifier === selectedId;
          return (
            <RuleCard
              title={rule.title}
              key={identifier}
              cardinality={rule.cardinality}
              identifier={identifier}
              selected={selected}
              interactable={interactable}
              canEdit={canEditRules}
              onEditClick={() => handleEditModalOpen(rule)}
              onDestroyClick={() => handleDestroyModalOpen(rule)}
            >
              {rule.description}
            </RuleCard>
          );
        })}
      </ol>
    );
  }

  return (
    <StyledRules>
      <Helmet>
        <title>Rules - Knockout!</title>
      </Helmet>
      <h1 className="underline">{resource !== '' ? `${resource} Rules` : `Rules`}</h1>
      <p className="detail">
        There are times where moderators will use their discretion and common sense to maintain a
        neutral, unbiased and fair stance. Think we’re not doing our job? Problem with a mute? You
        can contact us through the messaging system. Remember. Mutes are warnings. You are more than
        welcome to get an explanation if you don’t understand the warning.
      </p>
      {rulesContent}
      {canEditRules && selectedRule && (
        <>
          <EditRuleModal
            isOpen={editModalOpen}
            rule={selectedRule}
            closeModal={handleEditModalClose}
            submitFn={handleEditRule}
          />
          <Modal
            iconUrl="/static/icons/siren.png"
            title="Delete Rule"
            cancelFn={handleDestroyModalClose}
            submitFn={handleDestroyRule}
            isOpen={destroyModalOpen}
          />
        </>
      )}
    </StyledRules>
  );
};

export default Rules;
