import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import { transparentize } from 'polished';
import { ThemeHighlightWeaker, ThemeTextColor } from '../../utils/ThemeNew';

const RadioButton = ({ theme, checked }) => (
  <StyledRadioButton theme={theme && { mode: theme }} checked={checked}>
    <div className="radio-button-outer" />
    <div className="radio-button-inner" />
  </StyledRadioButton>
);

export default RadioButton;

RadioButton.propTypes = {
  theme: PropTypes.string,
  checked: PropTypes.bool.isRequired,
};

RadioButton.defaultProps = {
  theme: undefined,
};

const StyledRadioButton = styled.div`
  position: relative;
  width: 24px;
  height: 24px;
  display: inline-block;
  vertical-align: middle;
  margin-right: 8px;

  .radio-button-outer {
    border-radius: 50%;
    border: 2px solid
      ${(props) =>
        props.checked ? ThemeHighlightWeaker : transparentize(0.4, ThemeTextColor(props))};
    width: 100%;
    height: 100%;
    position: absolute;
    top: 0;
    left: 0;
    box-sizing: border-box;
    transition: 0.4s;
  }

  .radio-button-inner {
    position: absolute;
    top: 0;
    left: 0;
    border-radius: 50%;
    background: ${ThemeHighlightWeaker};
    width: 100%;
    height: 100%;
    box-sizing: border-box;
    transform: scale(0.5);
    opacity: ${(props) => (props.checked ? 1 : 0)};
    transition: 0.4s;
  }
`;
