import React, { useState } from 'react';
import styled from 'styled-components';
import uploadImage from '../../../services/images';
import Modal from '../../Modals/Modal';
import config from '../../../../config';
import { ThemeFontSizeLarge } from '../../../utils/ThemeNew';

const StyledUploadImageModal = styled(Modal)`
  label {
    input {
      width: 24px;
      height: ${ThemeFontSizeLarge};
      vertical-align: text-bottom;
    }
  }
`;

interface UploadImageModalProps {
  clipboardImage: Blob;
  modalOpen: boolean;
  setModalOpen: (open: boolean) => void;
  onUpload: (imageUrl: string, thumbnail: boolean) => void;
  onError: (error: string) => void;
}

export const UploadImageModal = ({
  clipboardImage,
  modalOpen,
  setModalOpen,
  onUpload,
  onError,
}: UploadImageModalProps) => {
  const [thumbnail, setThumbnail] = useState(false);

  const uploadImageToServer = async () => {
    try {
      const response = await uploadImage({ imageBlob: clipboardImage });
      if (response && response.data && response.data.fileName) {
        const imageHttpUrl = `${config.cdnHost}/image/${response.data.fileName}`;
        onUpload(imageHttpUrl, thumbnail);
      } else {
        onError((response as any).data.message);
      }
    } catch (error) {
      onError(error.message);
    }
    setThumbnail(false);
    setModalOpen(false);
  };

  return (
    <StyledUploadImageModal
      iconUrl="/static/icons/image-file.png"
      title="Upload Image"
      submitText="Upload and Insert"
      submitFn={uploadImageToServer}
      cancelFn={() => setModalOpen(false)}
      isOpen={modalOpen}
    >
      <div>
        <label>
          <input type="checkbox" checked={thumbnail} onChange={() => setThumbnail(!thumbnail)} />
          Thumbnail (max 300x300px)
        </label>
      </div>
    </StyledUploadImageModal>
  );
};

export default UploadImageModal;
