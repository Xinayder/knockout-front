/* eslint-disable react/jsx-curly-newline */
/* eslint-disable no-restricted-globals */
import React, { useState, useEffect, useRef } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

import EditorToolbar from './components/EditorToolbar';
import EditorOptions from './components/EditorOptions';
import UserSearchPopup from './components/UserSearchPopup';
import StyledEditorBB from './style';
import Tooltip from '../Tooltip';

import { handlePaste, checkForHotkeys } from './helpers/insertion';
import { insertMention, MENTION_CHAR } from './helpers/mention';
import { getPopupPosition } from './helpers/popup';
import {
  loadExtraScriptingSettingFromStorage,
  saveExtraScriptingInfoSettingToStorage,
  loadDisplayCountrySettingFromStorage,
  saveDisplayCountryInfoSettingToStorage,
} from '../../utils/postOptionsStorage';
import ErrorBoundary from '../ErrorBoundary';
import Parser from '../KnockoutBB/Parser';
import UploadImageModal from './components/UploadImageModal';
import { pushSmartNotification } from '../../utils/notification';

const EditorBB = ({ content, setContent, handleSubmit, editable, children }) => {
  const [allowExtraScripting, setAllowExtraScripting] = useState(
    loadExtraScriptingSettingFromStorage()
  );
  const [sendCountryInfo, setSendCountryInfo] = useState(loadDisplayCountrySettingFromStorage());
  const [selectionRange, setSelectionRange] = useState([]);
  const [showOptions, setShowOptions] = useState(false);
  const [preview, setPreview] = useState(false);
  const [caretPosition, setCaretPosition] = useState(0);
  const [showUserPopup, setShowUserPopup] = useState(null);
  const [popupPosition, setPopupPosition] = useState({ top: 0, left: 0 });
  const [popupSize, setPopupSize] = useState({ width: 0, height: 0 });
  const [mentionedUsers, setMentionedUsers] = useState([]);
  const [showImagePasteModal, setShowImagePasteModal] = useState(false);
  const [clipboardImage, setClipboardImage] = useState(null);
  const [handleImageUpload, setHandleImageUpload] = useState(null);

  const inputRef = useRef();

  const updateHeight = () => {
    const padding = 16;
    const numLineBreaks = (inputRef.current.value.match(/\n/g) || []).length + 1;
    const lineHeight = Number(getComputedStyle(inputRef.current).fontSize.slice(0, -2)) * 1.2;
    const newHeight = Math.max(numLineBreaks * lineHeight + padding, 120);
    inputRef.current.style.height = `${newHeight}px`;
  };

  const updateContent = (e) => {
    setContent(e.target.value);
    setCaretPosition(e.target.selectionStart);
    updateHeight();
  };

  const setSelection = (e) => {
    setSelectionRange([e.target.selectionStart, e.target.selectionEnd]);
  };

  const checkForMentionKey = (e) => {
    const inputValue = e.target.value;
    const cursorPosition = e.target.selectionStart;
    const insertedChar = inputValue[cursorPosition - 1];

    if (insertedChar === MENTION_CHAR) {
      setShowUserPopup(true);
    } else if (insertedChar === ' ') {
      setShowUserPopup(false);
    }
  };

  const preActionCheck = (nextFn) => {
    if (allowExtraScripting) {
      nextFn();
    }
  };

  const handleImagePaste = (clipboardContent, callback) => {
    if (!clipboardContent) {
      return;
    }

    setClipboardImage(clipboardContent);
    setShowImagePasteModal(true);
    setHandleImageUpload(() => {
      return (imageUrl, thumbnail) => {
        pushSmartNotification({ message: 'Image uploaded successfully!' });
        callback(imageUrl, thumbnail);
      };
    });
  };

  const handleImageUploadError = (error) => {
    pushSmartNotification({ error: `Failed to upload image: ${error}` });
  };

  useEffect(() => {
    saveExtraScriptingInfoSettingToStorage(allowExtraScripting);
  }, [allowExtraScripting]);

  useEffect(() => {
    saveDisplayCountryInfoSettingToStorage(sendCountryInfo);
  }, [sendCountryInfo]);

  useEffect(() => {
    updateHeight();
  }, [inputRef]);

  useEffect(() => {
    setPopupPosition(getPopupPosition(inputRef.current, caretPosition, popupSize));
  }, [popupSize, caretPosition]);

  const renderPopup = () => {
    return showUserPopup ? (
      // eslint-disable-next-line react/forbid-dom-props
      <div className="popup" style={{ top: popupPosition.top, left: popupPosition.left }}>
        <UserSearchPopup
          caretPosition={caretPosition}
          content={content}
          onSizeChange={(size) => setPopupSize(size)}
          closePopup={() => setShowUserPopup(false)}
          selectUser={(user) => {
            insertMention(user, inputRef.current, content, setContent, setSelectionRange, true);
            setMentionedUsers((value) => [...value, user]);
          }}
        />
      </div>
    ) : null;
  };

  return (
    <StyledEditorBB>
      {preview ? (
        <div className="post-preview">
          <ErrorBoundary errorMessage="Invalid KnockoutBB code.">
            <Parser content={content} mentionUsers={mentionedUsers} />
          </ErrorBoundary>
        </div>
      ) : (
        <textarea
          name="content"
          aria-label="Content"
          rows={1}
          ref={inputRef}
          value={content}
          disabled={editable ? undefined : true}
          onChange={updateContent}
          onInput={checkForMentionKey}
          onKeyDown={(e) =>
            preActionCheck(() =>
              checkForHotkeys(
                e,
                content,
                setContent,
                selectionRange,
                setSelectionRange,
                inputRef.current,
                handleSubmit
              )
            )
          }
          onKeyUp={(e) =>
            preActionCheck(() => {
              setSelection(e);
            })
          }
          onPaste={(e) =>
            preActionCheck(() => handlePaste(e, setContent, selectionRange, handleImagePaste))
          }
          onClick={(e) => preActionCheck(() => setSelection(e))}
          onSelect={(e) => preActionCheck(() => setSelection(e))}
        />
      )}
      {editable && showOptions && (
        <EditorOptions
          allowExtraScripting={allowExtraScripting}
          setAllowExtraScripting={setAllowExtraScripting}
          sendCountryInfo={sendCountryInfo}
          setSendCountryInfo={setSendCountryInfo}
          handleOptionsClose={() => setShowOptions(false)}
        />
      )}
      {editable && (
        <div className="editor-footer">
          {allowExtraScripting && !preview ? (
            <EditorToolbar
              content={content}
              setContent={setContent}
              selectionRange={selectionRange}
              setSelectionRange={setSelectionRange}
              input={inputRef.current}
            />
          ) : (
            <div className="toolbar disabled">
              {!preview && (
                <Tooltip text="Extra scripting disabled!">
                  <button type="button" data-testid="scripting-disabled">
                    <i className="fa-solid fa-ghost" />
                  </button>
                </Tooltip>
              )}
            </div>
          )}
          <div className="actions">
            <Link className="help-link" to="/knockoutbb" target="_blank">
              <i className="fa-solid fa-question" />
              &nbsp;Formatting Help
            </Link>
            <button type="button" onClick={() => setShowOptions(!showOptions)}>
              <i className="fa-solid fa-cog" />
              &nbsp;Settings
            </button>
            <button type="button" onClick={() => setPreview((value) => !value)}>
              {preview ? (
                <>
                  <i className="fa-solid fa-pen" />
                  &nbsp;Edit
                </>
              ) : (
                <>
                  <i className="fa-solid fa-eye" />
                  &nbsp;Preview
                </>
              )}
            </button>
            {children}
          </div>
        </div>
      )}
      {clipboardImage && (
        <UploadImageModal
          clipboardImage={clipboardImage}
          modalOpen={showImagePasteModal}
          setModalOpen={setShowImagePasteModal}
          onUpload={handleImageUpload}
          onError={handleImageUploadError}
        />
      )}
      {renderPopup()}
    </StyledEditorBB>
  );
};

EditorBB.propTypes = {
  content: PropTypes.string.isRequired,
  setContent: PropTypes.func.isRequired,
  handleSubmit: PropTypes.func,
  editable: PropTypes.bool,
  children: PropTypes.element,
};

EditorBB.defaultProps = {
  handleSubmit: () => {},
  editable: true,
  children: undefined,
};

export default EditorBB;
