import { User } from 'knockout-schema';
import { SetStateAction } from 'react';
import { modifySelectionRange } from './selection';

export const MENTION_CHAR = '@';
export const MENTION_CHAR_KEYCODE = 50;

export const isMention = (src) => {
  return src.match(/@<(\d+;[^>]+)>/g);
};

/**
 * @returns position of first character of mention after the @
 */
function getMentionStartIndex(content: string, caretPosition: number) {
  let index = caretPosition - 1;

  while (index >= 0 && content[index] !== MENTION_CHAR) {
    index -= 1;
  }
  return index >= 0 ? index : -1;
}

/**
 * @returns the mention query without the @
 */
export function getMentionQuery(content: string, caretPosition: number) {
  const startIndex = getMentionStartIndex(content, caretPosition);
  return startIndex >= 0 ? content.substring(startIndex + 1, caretPosition) : '';
}

/**
 * @returns content without the mention placeholder
 */
function removeMentionPlaceholder(content: string, caretPosition: number) {
  const startIndex = getMentionStartIndex(content, caretPosition);
  if (startIndex >= 0) {
    const mentionQueryLength = getMentionQuery(content, caretPosition).length + 1; // +1 for the "@"
    const modifiedContent =
      content.substring(0, startIndex) + content.substring(startIndex + mentionQueryLength);
    const newCaretPosition = startIndex;
    return { updatedContent: modifiedContent, newCaret: newCaretPosition };
  }
  return { updatedContent: content, newCaret: caretPosition };
}

export const insertMention = (
  user: User,
  input: { selectionStart: number },
  content: string,
  setContent: (value: string) => void,
  setSelectionRange: (value: SetStateAction<number[]>) => void,
  removePlaceholder = false
) => {
  const updatedContent = `${MENTION_CHAR}<${user.id}>`;
  const mentionLength = updatedContent.length;
  const caretPosition = input.selectionStart;

  let newContent = content;
  let newCaretPosition = caretPosition;

  if (removePlaceholder) {
    const { updatedContent: modifiedContent, newCaret } = removeMentionPlaceholder(
      content,
      caretPosition
    );
    newContent = modifiedContent;
    newCaretPosition = newCaret;
  }

  newContent =
    newContent.substring(0, newCaretPosition) +
    updatedContent +
    newContent.substring(newCaretPosition);

  setContent(newContent);
  newCaretPosition += mentionLength;
  setSelectionRange([newCaretPosition, newCaretPosition]);
  modifySelectionRange(input, newCaretPosition);
};
