import React, { useState } from 'react';
import { Rule } from 'knockout-schema';
import submitReport, { MAX_REPORT_REASON_LENGTH } from '../../services/reports';
import { pushSmartNotification } from '../../utils/notification';
import Modal from '../Modals/Modal';
import { FieldLabelSmall, TextField } from '../FormControls';
import RulesDropdown from './RulesDropdown';

interface ReportModalProps {
  isOpen: boolean;
  close: () => void;
  postId: number | undefined;
  rules: Rule[];
}

const ReportModal = ({ isOpen, close, postId, rules }: ReportModalProps) => {
  const [rule, setRule] = useState('');
  const [details, setDetails] = useState('');

  const submit = async () => {
    if (postId === undefined) {
      return;
    }

    if (rule === '') {
      pushSmartNotification({ error: 'A rule must be selected.' });
      return;
    }

    let reportReason = rule;
    if (details !== '') {
      reportReason += ` - ${details}`;
    }

    try {
      await submitReport({ postId, reportReason });
      close();
      setRule('');
      setDetails('');
    } catch (err) {
      pushSmartNotification({ error: err.message });
    }
  };

  let subforumId;
  const subforumRules = rules.filter((r) => r.rulableType === 'Subforum');
  if (subforumRules.length > 0) {
    subforumId = subforumRules[0].rulableId;
  }

  return (
    <Modal
      iconUrl="/static/icons/siren.png"
      title="Report post"
      cancelFn={() => {
        close();
        setRule('');
      }}
      submitFn={submit}
      isOpen={isOpen}
    >
      <FieldLabelSmall>
        {`Which of the `}
        <a href="/rules" target="_blank">
          site rules
        </a>
        {subforumId && ` or `}
        {subforumId && (
          <a href={`/subforumRules/${subforumId}`} target="_blank">
            subforum rules
          </a>
        )}
        {` did this post break?`}
      </FieldLabelSmall>
      <RulesDropdown rules={rules} onSelect={(e) => setRule(e.target.value)} />
      <FieldLabelSmall>Add more details (optional)</FieldLabelSmall>
      <TextField
        maxLength={MAX_REPORT_REASON_LENGTH - rule.length}
        value={details}
        onChange={(e) => setDetails(e.target.value)}
      />
    </Modal>
  );
};

export default ReportModal;
