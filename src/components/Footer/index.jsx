import React from 'react';
import styled from 'styled-components';
import { Link } from 'react-router-dom';

import {
  ThemeFontSizeSmall,
  ThemeVerticalPadding,
  ThemeHorizontalPadding,
  ThemeTextColor,
} from '../../utils/ThemeNew';
import StyleableLogo from '../Header/components/StyleableLogo';
import LoggedInOnly from '../LoggedInOnly';

const StyledFooter = styled.footer`
  position: relative;
  width: 100%;
  bottom: 0;
  display: flex;
  justify-content: space-between;
  align-items: center;

  margin: 0;
  padding-top: ${ThemeVerticalPadding};
  padding-bottom: ${ThemeVerticalPadding};
  padding-left: ${ThemeHorizontalPadding};
  padding-right: ${ThemeHorizontalPadding};
  font-size: ${ThemeFontSizeSmall};
  box-sizing: border-box;

  .logo {
    width: auto;
    height: 40px;
    margin-right: 10px;
    padding: 5px 0;
    position: relative;
    top: 0;
    shape-rendering: geometricprecision;
    transition: transform 300ms ease-in-out;
  }

  .linkContainer {
    display: flex;
    flex-wrap: wrap;
    flex-direction: row;
    justify-content: space-evenly;
    vertical-align: middle;
    padding-right: 20px;

    a,
    span,
    button {
      display: flex;
      align-items: center;
      color: ${ThemeTextColor};
      text-decoration: none;
      opacity: 0.75;
      transition: opacity 100ms ease-in-out;
      margin-left: 30px;
      cursor: pointer;
      &:hover {
        opacity: 1;
      }
    }

    button {
      background-color: transparent;
      padding: 0;
      display: inherit;
      font-size: inherit;
      font-family: inherit;
      border: none;
      &:focus {
        outline: none;
      }
    }
  }

  i {
    padding-right: 5px;
  }

  @media (max-width: 900px) {
    padding-bottom: 55px;
    margin-top: 0px;
    .no-tablet {
      display: none !important;
    }
  }

  @media (max-width: 700px) {
    margin-top: 0px;
    padding-bottom: 50px;
    flex-direction: column-reverse;
    justify-content: space-evenly;

    .no-mobile {
      display: none !important;
    }

    .linkContainer {
      width: 100%;
      margin-bottom: 15px;
      padding-right: 0px;
      justify-content: center;

      a,
      span {
        margin: ${ThemeVerticalPadding} ${ThemeHorizontalPadding};
      }

      button:first-child {
        margin-left: 0px;
      }
    }
  }
`;

const scrollToTop = () => {
  document.documentElement.scrollTop = 0;
};

const Footer = () => {
  return (
    <StyledFooter>
      <Link to="/">
        <StyleableLogo className="logo" />
      </Link>

      <div className="linkContainer">
        <button onClick={scrollToTop} type="button">
          <i className="fa-solid fa-level-up-alt" />
          &nbsp;Top
        </button>

        <Link to="/rules">
          <i className="fa-solid fa-atlas" />
          &nbsp;Rules
        </Link>

        <Link to="/communityteam">
          <i className="fa-solid fa-users" />
          &nbsp;Community Team
        </Link>

        <Link to="/changelog">
          <i className="fa-solid fa-code-branch" />
          &nbsp;Changelog
        </Link>

        <Link to="/calendar">
          <i className="fa-solid fa-calendar" />
          &nbsp;Calendar
        </Link>

        <LoggedInOnly>
          <Link to="/subscriptions" className="no-mobile no-tablet">
            <i className="fa-solid fa-newspaper" />
            &nbsp;Subscriptions
          </Link>
        </LoggedInOnly>

        <Link to="/threadsearch" className="no-mobile no-tablet">
          <i className="fa-solid fa-search" />
          &nbsp;Search
        </Link>

        <a href="https://steamcommunity.com/groups/knockoutchat" target="_blank" rel="noopener">
          <i className="fa-brands fa-steam" />
          &nbsp;Steam
        </a>

        <a href="https://discord.gg/wjWpapC" target="_blank" rel="noopener">
          <i className="fa-brands fa-discord" />
          &nbsp;Discord
        </a>

        <a href="https://gitlab.com/knockout-community" target="_blank" rel="noopener">
          <i className="fa-solid fa-code" />
          &nbsp;Source
        </a>

        <a href="https://icons8.com/" target="_blank" rel="noopener">
          <i className="fa-solid fa-external-link-alt" />
          &nbsp;Icons by Icons8
        </a>

        <a href="mailto:admin@knockout.chat" target="_blank" rel="noopener">
          <i className="fa-solid fa-envelope" />
          &nbsp;Contact
        </a>
      </div>
    </StyledFooter>
  );
};

export default Footer;
