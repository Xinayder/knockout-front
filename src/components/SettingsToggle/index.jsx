import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import { FieldLabelWithIcon, FieldLabelSmall } from '../FormControls';
import FormSwitch from '../FormControls/components/FormSwitch';
import { ThemeHorizontalPadding } from '../../utils/ThemeNew';

const StyledSettingsToggle = styled.label`
  display: flex;
  justify-content: space-between;
  line-height: normal;
  align-items: center;
  margin-bottom: 20px;
  padding: 0 20px;

  ${FieldLabelWithIcon} {
    margin-bottom: 5px;
    font-weight: 600;
  }

  ${FieldLabelSmall} {
    margin-bottom: 0px;
  }

  .switch-container {
    margin-left: calc(${ThemeHorizontalPadding} * 2);
  }
`;

const SettingsToggle = ({ label, desc, value, setValue, icon }) => (
  <StyledSettingsToggle>
    <div>
      <FieldLabelWithIcon label={label} icon={icon} />

      <FieldLabelSmall>{desc}</FieldLabelSmall>
    </div>
    <div className="switch-container">
      <FormSwitch checked={value} toggle={setValue} />
    </div>
  </StyledSettingsToggle>
);

SettingsToggle.propTypes = {
  label: PropTypes.string.isRequired,
  desc: PropTypes.string.isRequired,
  value: PropTypes.bool.isRequired,
  setValue: PropTypes.func.isRequired,
  icon: PropTypes.string,
};

SettingsToggle.defaultProps = {
  icon: '',
};

export default SettingsToggle;
