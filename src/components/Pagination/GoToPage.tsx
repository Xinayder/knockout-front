import React, { useRef, useState } from 'react';
import { useHistory } from 'react-router-dom';
import styled from 'styled-components';
import { ThemeFontSizeMedium, ThemeVerticalPadding } from '../../utils/ThemeNew';
import useDropdownMenu, {
  DropdownMenuButton,
  DropdownMenuOpened,
} from '../Header/components/DropdownMenu';
import { TabletMediaQuery } from '../SharedStyles';

const StyledDropdownMenu = styled(DropdownMenuOpened)`
  top: 25px;
  right: unset;
  left: 0;
  min-width: 100px;
  padding: ${ThemeVerticalPadding};
  font-size: ${ThemeFontSizeMedium};
  flex-direction: row;
  align-items: center;

  ${TabletMediaQuery} {
    width: unset;
    transform: unset;
    position: absolute;
  }

  .page-input-form {
    display: flex;
    align-items: center;
  }

  .page-input-text {
    white-space: nowrap;
    margin-right: 10px;
  }

  .page-input {
    width: 30px;
    margin-right: 5px;
  }
`;

interface GoToPageProps {
  currentPage: number;
  totalPages: number;
  className?: string;
  pageChangeFn: (page: number) => void;
  pagePath?: string;
}

const GoToPage = ({
  currentPage,
  totalPages,
  pageChangeFn,
  className = '',
  pagePath = '',
}: GoToPageProps) => {
  const menuRef = useRef<HTMLDivElement>(null);
  const buttonRef = useRef<HTMLButtonElement>(null);
  const [open, setOpen] = useDropdownMenu(menuRef, buttonRef);
  const [pageInput, setPageInput] = useState(String(currentPage));
  const history = useHistory();

  const onSubmit = (e) => {
    e.preventDefault();
    const value = Number(pageInput);
    if (value > 0 && value <= totalPages) {
      if (pagePath) {
        history.push(`${pagePath}${value}`);
      } else {
        pageChangeFn(value);
      }

      setOpen(false);
    }
  };
  return (
    <DropdownMenuButton
      as="button"
      type="button"
      ref={buttonRef}
      onClick={() => setOpen(!open)}
      className={className}
      title="Go to page"
    >
      <div className="dropdown-menu-button-inner">...</div>
      {open && (
        <StyledDropdownMenu onClick={(e) => e.stopPropagation()} ref={menuRef}>
          <form onSubmit={onSubmit} className="page-input-form">
            <label className="page-input-text" htmlFor="page-input">
              Go to page
            </label>
            <input
              className="page-input"
              value={pageInput}
              type="number"
              min="1"
              max={totalPages}
              onChange={(e) => setPageInput(e.target.value)}
            />
            <button type="submit">Go</button>
          </form>
        </StyledDropdownMenu>
      )}
    </DropdownMenuButton>
  );
};

export default GoToPage;
