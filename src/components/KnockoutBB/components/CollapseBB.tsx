import React, { useState } from 'react';
import styled from 'styled-components';

import {
  ThemeHorizontalPadding,
  ThemeVerticalPadding,
  ThemeBackgroundLighter,
  ThemeFontSizeMedium,
} from '../../../utils/ThemeNew';

interface StyledCollapsibleProps {
  title?: string;
  children?: React.ReactNode;
}

const StyledCollapsible = styled.div`
  background-color: ${ThemeBackgroundLighter};
  border: 1px solid rgba(0, 0, 0, 0.4);
  overflow: hidden;
  position: relative;
  margin: ${ThemeVerticalPadding} 0;

  .title {
    display: block;
    padding: ${ThemeHorizontalPadding} calc(${ThemeHorizontalPadding} * 1.5);
    font-size: ${ThemeFontSizeMedium};
    cursor: pointer;
    transition: background-color 100ms;
    &:hover {
      background-color: rgba(255, 255, 255, 0.3);
    }
    .chevron {
      display: inline-block;
      user-select: none;
      width: ${ThemeFontSizeMedium};
      padding-right: 0.3em;
    }
  }

  .body {
    display: none;
    margin: calc(${ThemeHorizontalPadding} * 1.5);
    overflow: hidden;
    position: relative;
  }

  &.expanded {
    > .title {
      border-bottom: 1px solid rgba(0, 0, 0, 0.4);
      background-color: rgba(0, 0, 0, 0.2);
    }

    > .body {
      display: block;
    }
  }
`;

const CollapsibleBB = ({ title = undefined, children = undefined }: StyledCollapsibleProps) => {
  const [expanded, setExpanded] = useState(false);

  return (
    <StyledCollapsible className={`${expanded ? 'expanded' : ''} collapsible-box`}>
      <div
        className="title"
        onClick={() => setExpanded(!expanded)}
        onKeyDown={() => setExpanded(!expanded)}
        role="button"
        tabIndex={0}
        title={expanded ? 'Click to collapse' : 'Click to expand'}
      >
        <div className="chevron">
          {expanded ? (
            <i className="fa-solid fa-chevron-down" />
          ) : (
            <i className="fa-solid fa-chevron-right" />
          )}
        </div>
        {title === undefined || title.match(/^\s*$/) ? 'Expand:' : title}
      </div>
      <div className="body">{children}</div>
    </StyledCollapsible>
  );
};

export default CollapsibleBB;
