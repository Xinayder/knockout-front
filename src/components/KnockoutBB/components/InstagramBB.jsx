import React, { useRef } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import useAsyncScript from '../../../utils/useAsyncScript';

export const isInstagram = (src) => {
  try {
    const url = new URL(src);
    if (url.hostname !== 'www.instagram.com') throw new Error();
    return true;
  } catch (error) {
    return null;
  }
};

const InstagramWrapper = styled.div`
  .instagram-media {
    background: #fff;
    border: 0;
    border-radius: 3px;
    box-shadow: 0 0 1px 0 rgba(0, 0, 0, 0.5), 0 1px 10px 0 rgba(0, 0, 0, 0.15);
    margin: 1px;
    max-width: 540px;
    min-width: 326px;
    padding: 0;
    width: 99.375%;
    width: -webkit-calc(100% - 2px);
    width: calc(100% - 2px);
  }

  .inner {
    padding: 16px;
  }

  .link {
    background: #ffffff;
    line-height: 0;
    padding: 0 0;
    text-align: center;
    text-decoration: none;
    width: 100%;
  }
`;
const InstagramBB = ({ href, children }) => {
  const ref = useRef();
  useAsyncScript('//www.instagram.com/embed.js', 'instagram-script');
  if (window.instgrm) window.instgrm.Embeds.process();
  const url = href || children.join('');

  return (
    <InstagramWrapper ref={ref}>
      <blockquote
        className="instagram-media"
        data-instgrm-captioned
        data-instgrm-permalink={url}
        data-instgrm-version="13"
      >
        <div className="inner">
          <a className="link" href={url} target="_blank">
            Post
          </a>
        </div>
      </blockquote>
    </InstagramWrapper>
  );
};

InstagramBB.propTypes = {
  href: PropTypes.string.isRequired,
  children: PropTypes.string.isRequired,
};

export default InstagramBB;
