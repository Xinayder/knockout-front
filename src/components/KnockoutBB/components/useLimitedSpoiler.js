import dayjs from 'dayjs';
import { useContext, useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import PostContext from '../../Post/PostContext';
import {
  loadApprovedUserIdsFromStorage,
  setApprovedUserIdsToStorage,
} from '../../../services/theme';

const isLimitedUser = (user, postEdited, isUserApproved) => {
  const newUser = dayjs(user.createdAt).add(2, 'week').isAfter(dayjs());
  const lowPostCount = user.posts < 15;
  return (
    (newUser || lowPostCount) &&
    dayjs(postEdited).add(18, 'hour').isAfter(dayjs()) &&
    !isUserApproved
  );
};

export default (spoiler) => {
  const { user, postEdited } = useContext(PostContext);
  const [approvedUserIds, setApprovedUserIds] = useState(new Set());
  const hideRiskyMedia = useSelector((state) => state.settings.hideRiskyMedia);
  const limitedUser = isLimitedUser(user, postEdited, approvedUserIds.has(user.id));
  const spoilerLimitedUserMedia = limitedUser && hideRiskyMedia;
  const [spoilered, setSpoilered] = useState(spoiler || spoilerLimitedUserMedia);

  useEffect(() => {
    setApprovedUserIds(new Set(loadApprovedUserIdsFromStorage()));
  }, []);

  useEffect(() => {
    setSpoilered(spoiler || spoilerLimitedUserMedia);
  }, [user, spoilerLimitedUserMedia, spoiler]);

  useEffect(() => {
    if (!spoilered && spoilerLimitedUserMedia) {
      // user has unspoilerd a media element for the limited user that wasn't spoilered otherwise,
      // so remember the users ID, that way we don't set this element as spoilered for the user ID again
      setApprovedUserIdsToStorage([...approvedUserIds, user.id]);
    }
  }, [spoilered, spoilerLimitedUserMedia, approvedUserIds, user.id]);

  const updateApprovedUserIds = () => {
    setApprovedUserIds(new Set(loadApprovedUserIdsFromStorage()));
  };

  useEffect(() => {
    window.addEventListener('approvedUserIds', updateApprovedUserIds);

    return () => {
      window.removeEventListener('approvedUserIds', updateApprovedUserIds);
    };
  }, []);

  const reveal = (setModalOpen) => {
    if (limitedUser) {
      setModalOpen(true);
    } else {
      setSpoilered(false);
    }
  };

  return [spoilered, setSpoilered, reveal, limitedUser];
};
