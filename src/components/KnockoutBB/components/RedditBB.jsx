import React, { useContext } from 'react';
import PropTypes from 'prop-types';
import { ThemeContext } from 'styled-components';
import useAsyncScript from '../../../utils/useAsyncScript';

export const isReddit = (src) => {
  try {
    const url = new URL(src);
    if (url.hostname !== 'www.reddit.com') throw new Error();
    return true;
  } catch (error) {
    return null;
  }
};

const RedditBB = ({ href, children }) => {
  const url = new URL(href || children.join(''));
  const theme = useContext(ThemeContext);

  useAsyncScript('https://embed.reddit.com/widgets.js');

  return (
    <blockquote
      className="reddit-embed-bq"
      // eslint-disable-next-line react/forbid-dom-props
      style={{ height: '500px' }}
      data-embed-theme={theme.mode !== 'light' ? 'dark' : ''}
      data-embed-height="500"
      loading="lazy"
    >
      <a href={url}>Reddit embed</a>
    </blockquote>
  );
};

RedditBB.propTypes = {
  href: PropTypes.string.isRequired,
  children: PropTypes.string.isRequired,
};

export default RedditBB;
