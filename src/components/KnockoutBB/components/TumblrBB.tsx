import React, { useEffect, useState } from 'react';
import jsonp from 'jsonp';
import useAsyncScript from '../../../utils/useAsyncScript';

export const getTumblrId = (src) => {
  try {
    const url = new URL(src);
    if (!url.hostname.includes('tumblr.com')) throw new Error();
    return url.pathname.split('/')[2];
  } catch (error) {
    return null;
  }
};

interface TumblrBBProps {
  href?: string;
  children?: React.ReactNode[];
}
const TumblrBB = ({ href = undefined, children = undefined }: TumblrBBProps) => {
  const url = href || children?.join('');
  const [embedInfo, setEmbedInfo] = useState({ dataId: '', dataHref: '' });
  const [error, setError] = useState(false);

  useAsyncScript('https://assets.tumblr.com/post.js', 'tumblr-post', undefined, [embedInfo]);

  // retrieve an attribute from an HTML string
  const getAttr = (attr, html) => {
    const reg = new RegExp(`${attr}="([^"]+)"`, 'g');
    const match = reg.exec(html);
    return match ? match[1] : '';
  };

  useEffect(() => {
    jsonp(`https://www.tumblr.com/oembed/1.0?url=${url}&format=json`, null, (err, data) => {
      if (err) {
        console.error(err);
        setError(true);
      } else {
        const { html } = data.response;
        setEmbedInfo({ dataId: getAttr('data-did', html), dataHref: getAttr('data-href', html) });
      }
    });
  }, []);

  if (error) return <span>Error loading Tumblr post.</span>;

  return (
    embedInfo.dataId && (
      <div className="tumblr-post" data-href={embedInfo.dataHref} data-did={embedInfo.dataId}>
        <a href={url}>{url}</a>
      </div>
    )
  );
};

export default TumblrBB;
