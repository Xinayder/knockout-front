import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';

import { ThemeTextColor } from '../../../utils/ThemeNew';

const StyledSpoiler = styled.span`
  ${(props) => !props.hasChildNodes && `background: ${ThemeTextColor(props)};`}
  color: ${ThemeTextColor};

  span,
  h1,
  h2,
  a,
  b,
  s,
  u,
  code,
  li,
  i:not(.fa-eye-slash) {
    color: ${ThemeTextColor};
    background: ${ThemeTextColor};

    a {
      background: ${ThemeTextColor};
    }
  }

  div.quotebox {
    a {
      background: ${ThemeTextColor};
    }

    .body .content {
      color: ${ThemeTextColor};
      background: ${ThemeTextColor};
    }
  }

  a {
    color: ${ThemeTextColor};
  }

  &:hover {
    background: transparent;

    span,
    h1,
    h2,
    a,
    b,
    s,
    u,
    code,
    li,
    i:not(.fa-eye-slash) {
      background: transparent;

      a {
        background: transparent;
      }
    }

    div.quotebox .body .content {
      background: transparent;
    }
  }
`;

const SpoilerBB = ({ children, hasChildNodes }) => (
  <StyledSpoiler hasChildNodes={hasChildNodes}>{children}</StyledSpoiler>
);

SpoilerBB.propTypes = {
  children: PropTypes.node.isRequired,
  hasChildNodes: PropTypes.bool,
};

SpoilerBB.defaultProps = {
  hasChildNodes: false,
};

export default SpoilerBB;
