import React, { useEffect, useState } from 'react';
import styled from 'styled-components';
import { darken, transparentize } from 'polished';
import dayjs from 'dayjs';
import localizedFormat from 'dayjs/plugin/localizedFormat'; // ES 2015
import tweetEmbed from '../../../services/tweetEmbed';
import {
  ThemeBackgroundDarker,
  ThemeFontSizeLarge,
  ThemeFontSizeMedium,
  ThemeFontSizeSmall,
  ThemeTextColor,
} from '../../../utils/ThemeNew';
import LinkBB from './LinkBB';
import { TabletMediaQuery } from '../../SharedStyles';

dayjs.extend(localizedFormat);

export const getTweetId = (src) => {
  const twitterRegx =
    /^https?:\/\/(?:mobile\.)?(?:twitter|x)(?:\.com)\/(?:#!\/)?(\w+)\/status(es)?\/(\d+)/;

  const tweet = twitterRegx.exec(src);
  if (!tweet) return null;
  const tweetId = tweet[3];
  if (!tweetId) return null;

  return tweetId;
};

const TweetContainer = styled.div`
  border: 1px solid ${(props) => transparentize(0.7, ThemeTextColor(props))};
  padding: 16px;
  margin: 16px 0;
  background-color: ${(props) =>
    props.theme.mode === 'light'
      ? darken(0.07, ThemeBackgroundDarker(props))
      : darken(0.02, ThemeBackgroundDarker(props))};
  max-width: 35%;

  ${TabletMediaQuery} {
    max-width: 100%;
  }

  &:not(.tweet-video) {
    cursor: pointer;
  }

  .tweet-header {
    display: flex;
    align-items: center;
    margin-bottom: 12px;
  }

  .tweet-source {
    font-size: ${ThemeFontSizeMedium};
    color: #657786;
    margin-left: 6px;
    white-space: nowrap;
  }

  .tweet-content {
    display: flex;
    flex-direction: column;
  }

  .author-name {
    font-size: ${ThemeFontSizeLarge};
    font-weight: bold;
    display: inline-block;
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
  }

  .author-image {
    border-radius: 50%;
    width: 32px;
    margin-right: 8px;
  }

  .tweet-text {
    font-size: ${ThemeFontSizeLarge};
    margin: 0 0 8px 0;
  }

  .tweet-image {
    width: 100%;
    border-radius: 8px;
    margin: 8px 0;
  }

  .tweet-video {
    max-width: 100%;
    border-radius: 8px;
    margin: 8px 0;
  }

  .loading-text {
    font-size: ${ThemeFontSizeMedium};
    color: ${(props) => transparentize(0.5, ThemeTextColor(props))};
  }

  .tweet-footer {
    font-size: ${ThemeFontSizeSmall};
    color: ${(props) => transparentize(0.5, ThemeTextColor(props))};
    margin-top: 4px;
  }

  .tweet-reply {
    margin-bottom: 12px;

    .tweet-text {
      font-size: ${ThemeFontSizeMedium};
    }

    & > .tweet-content > .tweet-body {
      border-left: 2px solid ${(props) => transparentize(0.7, ThemeTextColor(props))};
      padding-left: 20px;
      margin-left: 15px;
      padding-bottom: 24px;
    }
  }

  .tweet-quote {
    border: 1px solid ${(props) => transparentize(0.7, ThemeTextColor(props))};
    padding: 12px;
    margin-top: 6px;

    .tweet-text {
      font-size: ${ThemeFontSizeMedium};
    }

    .author-name {
      font-size: ${ThemeFontSizeMedium};
    }

    .author-image {
      width: 24px;
    }
  }
`;

interface TweetBBProps {
  href?: string;
  children?: React.ReactNode[];
}
const TweetBB = ({ href = '', children = [] }: TweetBBProps) => {
  const tweetUrl = href || children.join('');
  const [tweetData, setTweetData] = useState<TweetAPIResponse | null>(null);

  const getEmbed = async () => {
    const response: TweetAPIResponse = await tweetEmbed(tweetUrl);
    setTweetData(response);
  };

  useEffect(() => {
    getEmbed();
  }, [tweetUrl]);

  const handleClick = (event: React.MouseEvent | React.KeyboardEvent, tweet: APIStatus) => {
    if (!(event.target as HTMLElement).classList.contains('tweet-video')) {
      window.open(tweet.url, '_blank');
    }
  };

  const tweetRender = (tweet: APIStatus, quote = false) => {
    const tweetPhoto = tweet?.media?.photos?.[0];
    const tweetVideo = tweet?.media?.videos?.[0];

    return (
      <div
        className="tweet-content"
        role="button"
        tabIndex={0}
        onClick={(event) => handleClick(event, tweet)}
        onKeyPress={(event) => {
          if (event.key === 'Enter' || event.key === ' ') {
            handleClick(event, tweet);
          }
        }}
      >
        <div className="tweet-header">
          <img
            className="author-image"
            alt={`@${tweet?.author.screen_name}'s avatar`}
            src={tweet?.author.avatar_url}
          />
          <a className="author-name" href={tweet?.author.url}>
            {tweet?.author.name}
            &nbsp;
            {`(@${tweet?.author.screen_name})`}
          </a>
          {!quote && (
            <a className="tweet-source" href={tweet?.url}>
              &nbsp;on Twitter
            </a>
          )}
        </div>
        <div className="tweet-body">
          <p className="tweet-text">{tweet?.text}</p>
          {tweetPhoto && (
            <img
              className="tweet-image"
              key={tweetPhoto.url}
              src={tweetPhoto.url}
              alt={tweetPhoto.altText}
            />
          )}
          {tweetVideo && (
            <video className="tweet-video" key={tweetVideo.url} controls>
              <source src={tweetVideo.url} type={tweetVideo.format} />
              Your browser does not support the video tag.
            </video>
          )}
          {tweet.quote && <div className="tweet-quote">{tweetRender(tweet.quote, true)}</div>}
        </div>
      </div>
    );
  };

  // strip query params and hash from URL
  const urlObj = new URL(tweetUrl);
  urlObj.search = '';
  urlObj.hash = '';

  return (
    <>
      <TweetContainer>
        {tweetData?.tweet ? (
          <>
            {tweetData.tweet.replying_to && (
              <div className="tweet-reply">{tweetRender(tweetData.tweet.replying_to, true)}</div>
            )}
            {tweetRender(tweetData.tweet!)}

            <div className="tweet-footer">
              {dayjs(tweetData.tweet.created_timestamp * 1000).format('LLL')}
            </div>
          </>
        ) : (
          <p className="loading-text">Loading...</p>
        )}
      </TweetContainer>
      <LinkBB href={urlObj.toString()} />
    </>
  );
};

export default TweetBB;
