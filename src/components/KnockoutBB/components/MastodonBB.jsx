/* eslint-disable react/forbid-dom-props */
import React, { useEffect, useRef } from 'react';
import PropTypes from 'prop-types';

export const isMastodon = (src) => {
  try {
    // mastodon urls always have a '@' before the username
    if (!src.includes('@')) throw new Error();
    // crappy regex to check if there's a slash followed by numbers
    if (!/([0-9]+)(?=[^/]*$)/gm.test(src)) throw new Error();
    return true;
  } catch (error) {
    return null;
  }
};

const MastodonBB = ({ href }) => {
  const container = useRef();
  let url = href;
  if (url.endsWith('/')) {
    url = url.slice(0, -1);
  }

  const frameId = url.split('/').pop();

  const parseHeight = (event) => {
    try {
      const data = event.data || {};
      if (typeof data === 'object' && data.type === 'setHeight' && data.id === frameId) {
        container.current.setAttribute('height', data.height);
      } else if (typeof data === 'number' && event.origin === new URL(url).origin) {
        container.current.setAttribute('height', data + 60);
      }
    } catch (error) {
      console.error(`Message parse failed: ${error}`);
    }
  };

  const onLoad = () => {
    container.current.contentWindow.postMessage(
      {
        type: 'setHeight',
        id: frameId,
      },
      '*'
    );
  };

  useEffect(() => {
    window.addEventListener('message', parseHeight);
    return () => window.removeEventListener('message', parseHeight);
  }, []);

  if (isMastodon(href)) {
    const newHref = `${href}/embed`;

    return (
      <iframe
        title="Mastodon Embed"
        src={newHref}
        ref={container}
        width="400"
        onLoad={onLoad}
        scrolling="no"
        style={{ border: 'none', maxWidth: '100%', overflow: 'hidden' }}
      />
    );
  }
  return '[Bad Mastodon embed.]';
};
MastodonBB.propTypes = {
  href: PropTypes.string.isRequired,
};

export default MastodonBB;
