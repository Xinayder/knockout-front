import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import dayjs from 'dayjs';
import {
  ThemeBackgroundLighter,
  ThemeHorizontalPadding,
  ThemeVerticalPadding,
} from '../../../utils/ThemeNew';
import { buttonHoverGray } from '../../SharedStyles';

const SmartLinkWrapper = styled.a`
  width: 100%;
  max-width: 450px;
  max-height: 150px;
  display: flex;
  box-sizing: border-box;
  padding: ${ThemeVerticalPadding} ${ThemeHorizontalPadding};
  background: ${ThemeBackgroundLighter};
  border-radius: 5px;
  overflow: hidden;
  cursor: pointer;
  position: relative;

  ${buttonHoverGray}
`;
const SmartLinkImageWrapper = styled.div`
  display: block;
  max-width: 100px;
  flex-shrink: 0;
  margin-right: 10px;
  z-index: 1;
`;
const SmartLinkImage = styled.img`
  display: block;
  max-width: 100%;
  height: auto;
  flex-shrink: 0;
`;
const SmartLinkInfo = styled.div`
  display: flex;
  flex-direction: column;
  width: 75%;
  /* height: 100%; */
  justify-content: space-between;
  z-index: 1;
  line-height: normal;
`;
const SmartLinkTitle = styled.b`
  font-weight: bold;
  width: 100%;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
  font-size: 14px;
`;
const SmartLinkDescription = styled.div`
  width: 100%;
  font-size: 11px;
  margin-bottom: 5px;

  overflow: hidden;
  text-overflow: ellipsis;
`;
const SmartLinkAuthor = styled.div`
  font-size: 10px;
  opacity: 0.9;
`;
const SmartLinkSmallInfo = styled.div`
  display: flex;
  justify-content: space-between;
  opacity: 0.8;
`;
const SmartLinkUrl = styled.div`
  font-size: 9px;
  display: inline-block;
  overflow: hidden;
  text-overflow: ellipsis;
  white-space: nowrap;
  max-width: 65%;
  color: #3facff;
`;
const SmartLinkDate = styled.div`
  font-size: 9px;
  display: inline-block;
`;
const SmartLinkBackground = styled.div`
  position: absolute;
  font-size: 35px;
  opacity: 0.03;
  z-index: 0;
  right: 5px;
  bottom: 5px;
`;

const SmartLink = ({ author, date, image, logo, title, url, description, linkUrl }) => (
  <SmartLinkWrapper href={linkUrl || url} target="_blank" rel="noopener ugc">
    {(image || logo) && (
      <SmartLinkImageWrapper>
        <SmartLinkImage src={image || logo} />
      </SmartLinkImageWrapper>
    )}
    <SmartLinkInfo>
      {title && <SmartLinkTitle>{title}</SmartLinkTitle>}
      {description && <SmartLinkDescription>{description}</SmartLinkDescription>}
      {author && <SmartLinkAuthor>{author}</SmartLinkAuthor>}
      {(url || date) && (
        <SmartLinkSmallInfo>
          {url && <SmartLinkUrl>{url}</SmartLinkUrl>}
          {date && <SmartLinkDate>{dayjs(date).format('DD/MM/YYYY HH:mm')}</SmartLinkDate>}
        </SmartLinkSmallInfo>
      )}
    </SmartLinkInfo>
    <SmartLinkBackground className="fa-solid fa-external-link-alt" />
  </SmartLinkWrapper>
);

SmartLink.propTypes = {
  linkUrl: PropTypes.string,
  author: PropTypes.string,
  date: PropTypes.string,
  description: PropTypes.string,
  image: PropTypes.string,
  logo: PropTypes.string,
  title: PropTypes.string,
  url: PropTypes.string,
};
SmartLink.defaultProps = {
  linkUrl: undefined,
  author: undefined,
  date: undefined,
  description: undefined,
  image: undefined,
  logo: undefined,
  title: undefined,
  url: undefined,
};

export default SmartLink;
