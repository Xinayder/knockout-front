/* eslint-disable react/prop-types */
import React, { useContext } from 'react';

import { loadEmbedSettingFromStorage } from '../../utils/thirdPartyEmbedStorage';
import VideoBB from './components/VideoBB';
import LinkBB from './components/LinkBB';
import ImageBB from './components/ImageBB';
import YoutubeBB from './components/YoutubeBB';
import TweetBB from './components/TweetBB';
import SpoilerBB from './components/SpoilerBB';
import QuoteBBLabs from './components/QuoteBB';
import EmbedWarning from '../EmbedWarning';
import CodeBB from './components/CodeBB';
import VimeoBB from './components/VimeoBB';
import StreamableBB from './components/StreamableBB';
import VocarooBB from './components/VocarooBB';
import SpotifyBB from './components/SpotifyBB';
import TwitchBB from './components/TwitchBB';
import SoundCloudBB from './components/SoundCloudBB';
import RedditBB from './components/RedditBB';
import InstagramBB from './components/InstagramBB';
import TikTokBB from './components/TikTokBB';
import PostContext from '../Post/PostContext';
import TumblrBB from './components/TumblrBB';
import MastodonBB from './components/MastodonBB';
import BlueskyBB from './components/BlueskyBB';
import CollapseBB from './components/CollapseBB';

const thirdPartyEmbedsEnabled = loadEmbedSettingFromStorage();

const Schema = ({ tag, content, properties, raw, spoiler, hasChildren }) => {
  const lowerTag = typeof tag === 'string' ? tag.toLowerCase() : null;
  const { profileView: smallEmbeds } = useContext(PostContext);
  switch (lowerTag) {
    case 'b': {
      return <b key={properties.key}>{content}</b>;
    }
    case 'i': {
      return <i key={properties.key}>{content}</i>;
    }
    case 'u': {
      return <u key={properties.key}>{content}</u>;
    }
    case 's': {
      return <s key={properties.key}>{content}</s>;
    }
    case 'blockquote': {
      return <blockquote key={properties.key}>{content}</blockquote>;
    }
    case 'spoiler': {
      return (
        <SpoilerBB key={properties.key} hasChildNodes={hasChildren}>
          {content}
        </SpoilerBB>
      );
    }
    case 'ul': {
      return <ul key={properties.key}>{content}</ul>;
    }
    case 'ol': {
      return <ol key={properties.key}>{content}</ol>;
    }
    case 'li': {
      return <li key={properties.key}>{content}</li>;
    }
    case 'h1': {
      return <h1 key={properties.key}>{content}</h1>;
    }
    case 'h2': {
      return <h2 key={properties.key}>{content}</h2>;
    }
    case 't':
    case 'img': {
      const { href, link, title, thumb, thumbnail } = properties;
      const isThumbnail =
        typeof thumb !== 'undefined' || typeof thumbnail !== 'undefined' || lowerTag === 't';
      const isLink = typeof link !== 'undefined';

      return (
        <ImageBB
          key={properties.key}
          href={href || content}
          thumbnail={isThumbnail || smallEmbeds}
          link={isLink}
          spoiler={spoiler}
          title={title}
        />
      );
    }
    case 'video': {
      const { href } = properties;
      return (
        <VideoBB
          key={properties.key}
          href={href || content}
          small={smallEmbeds}
          spoiler={spoiler}
        />
      );
    }
    case 'url': {
      const { href = null, smart } = properties;
      const isSmart = typeof smart !== 'undefined';
      return (
        <LinkBB key={properties.key} isSmart={isSmart} href={href || content}>
          {content || href}
        </LinkBB>
      );
    }
    case 'youtube': {
      const { href, thumbnail } = properties;
      const isThumbnail = typeof thumbnail !== 'undefined';
      return thirdPartyEmbedsEnabled ? (
        <YoutubeBB
          key={properties.key}
          href={href || content}
          small={smallEmbeds || isThumbnail}
          spoiler={spoiler}
        />
      ) : (
        <EmbedWarning service="YouTube" href={href || content} />
      );
    }
    case 'twitter': {
      const { href } = properties;
      return <TweetBB key={properties.key} href={href || content} />;
    }
    case 'q':
    case 'quote': {
      const { mentionsUser, postId, threadPage, threadId, username } = properties;
      return (
        <QuoteBBLabs
          key={properties.key}
          mentionsUser={mentionsUser}
          postId={postId}
          threadPage={threadPage}
          threadId={threadId}
          username={username}
        >
          {content}
        </QuoteBBLabs>
      );
    }
    case 'collapse': {
      const { title } = properties;
      return <CollapseBB title={title}>{content}</CollapseBB>;
    }
    case 'code': {
      const { language, inline } = properties;
      const isInline = typeof inline !== 'undefined';
      return (
        <CodeBB key={properties.key} language={language} inline={isInline}>
          {content}
        </CodeBB>
      );
    }
    case 'vimeo': {
      const { href } = properties;
      return thirdPartyEmbedsEnabled ? (
        <VimeoBB key={properties.key} href={href || content} spoiler={spoiler} />
      ) : (
        <EmbedWarning service="Vimeo" href={href || content} />
      );
    }
    case 'streamable': {
      const { href } = properties;
      return thirdPartyEmbedsEnabled ? (
        <StreamableBB key={properties.key} href={href || content} spoiler={spoiler} />
      ) : (
        <EmbedWarning service="Streamable" href={href || content} />
      );
    }
    case 'vocaroo': {
      const { href } = properties;
      return thirdPartyEmbedsEnabled ? (
        <VocarooBB key={properties.key} href={href || content} />
      ) : (
        <EmbedWarning service="Vocaroo" href={href || content} />
      );
    }
    case 'spotify': {
      const { href } = properties;
      return thirdPartyEmbedsEnabled ? (
        <SpotifyBB key={properties.key} href={href || content} />
      ) : (
        <EmbedWarning service="Spotify" href={href || content} />
      );
    }
    case 'twitch': {
      const { href } = properties;
      return thirdPartyEmbedsEnabled ? (
        <TwitchBB key={properties.key} href={href || content} />
      ) : (
        <EmbedWarning service="Twitch" href={href || content} />
      );
    }
    case `soundcloud`: {
      const { href } = properties;
      return thirdPartyEmbedsEnabled ? (
        <SoundCloudBB key={properties.key} href={href || content} />
      ) : (
        <EmbedWarning service="Soundcloud" href={href || content} />
      );
    }
    case `reddit`: {
      const { href } = properties;
      return thirdPartyEmbedsEnabled ? (
        <RedditBB key={properties.key} href={href || content} />
      ) : (
        <EmbedWarning service="Reddit" href={href || content} />
      );
    }
    case `instagram`: {
      const { href } = properties;
      return thirdPartyEmbedsEnabled ? (
        <InstagramBB key={properties.key} href={href || content} />
      ) : (
        <EmbedWarning service="Instagram" href={href || content} />
      );
    }
    case `tiktok`: {
      const { href } = properties;
      return thirdPartyEmbedsEnabled ? (
        <TikTokBB key={properties.key} href={href || content} />
      ) : (
        <EmbedWarning service="TikTok" href={href || content} />
      );
    }
    case `tumblr`: {
      const { href } = properties;
      return thirdPartyEmbedsEnabled ? (
        <TumblrBB key={properties.key} href={href || content} />
      ) : (
        <EmbedWarning service="Tumblr" href={href || content} />
      );
    }
    case `mastodon`: {
      const { href } = properties;
      return thirdPartyEmbedsEnabled ? (
        <MastodonBB key={properties.key} href={href || content} />
      ) : (
        <EmbedWarning service="Mastodon" href={href || content} />
      );
    }
    case 'bluesky': {
      const { href } = properties;
      return thirdPartyEmbedsEnabled ? (
        <BlueskyBB key={properties.key} href={href || content} />
      ) : (
        <EmbedWarning service="Bluesky" href={href || content} />
      );
    }
    case 'noparse': {
      return <span>{`${content}`}</span>;
    }
    default: {
      console.log(`You entered an unsupported tag: ${tag}. This could be a mistake on your end.`);
      return raw;
    }
  }
};

export default Schema;
