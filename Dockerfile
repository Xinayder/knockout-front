FROM node:20.10.0

WORKDIR /usr/src/server
COPY . .
RUN yarn && yarn run build:prod
CMD node expressServer.js
