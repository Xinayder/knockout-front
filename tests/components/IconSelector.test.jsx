/* eslint-disable no-underscore-dangle */
import React from 'react';
import { customRender, fireEvent, act, screen } from '../custom_renderer';
import '@testing-library/jest-dom';

import IconSelector from '../../src/components/IconSelector';
import store from '../../src/state/configureStore';

describe('IconSelector component', () => {
  let handleIconChangeMock;

  beforeEach(() => {
    localStorage.clear();
    handleIconChangeMock = jest.fn();
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  describe('As a standard logged in user', () => {
    const loggedInState = {
      user: { loggedIn: true, username: 'TestUser', role: { code: 'basic-user' } },
    };
    const userLocalStorageDetails = { id: 123, username: 'TestUser', role: { code: 'basic-user' } };

    beforeEach(() => {
      localStorage.__STORE__.currentUser = JSON.stringify(userLocalStorageDetails);
      jest.spyOn(store, 'getState').mockReturnValue(loggedInState);
    });

    test('I can see a list of all the icons available to me', async () => {
      await act(async () => {
        customRender(<IconSelector handleIconChange={handleIconChangeMock} />);
      });

      const dramaIcon = screen.getByTitle('Grocery');
      const healthIcon = screen.getByTitle('Health');

      expect(dramaIcon).toBeDefined();
      expect(healthIcon).toBeDefined();
    });

    test('I can click on an icon to select it', async () => {
      await act(async () => {
        customRender(<IconSelector handleIconChange={handleIconChangeMock} />);
      });

      const dramaIcon = screen.getByTitle('Grocery');
      fireEvent.click(dramaIcon);
      expect(handleIconChangeMock).toHaveBeenCalled();
    });

    test('I can click on a category to filter the icons down to a subset of icons', async () => {
      await act(async () => {
        customRender(<IconSelector handleIconChange={handleIconChangeMock} />);
      });

      const dramaIcon = screen.getByTitle('Grocery');
      const modernTvIcon = screen.getByTitle('Modern TV');
      const category = screen.getByTitle('Technology category');

      fireEvent.click(category);

      expect(dramaIcon).not.toBeVisible();
      expect(modernTvIcon).toBeVisible();
    });

    test('I can start typing a search term, and have the icons be filtered down by those matching the keyword', async () => {
      await act(async () => {
        customRender(<IconSelector handleIconChange={handleIconChangeMock} />);
      });

      const dramaIcon = screen.getByTitle('Grocery');
      const modernTvIcon = screen.getByTitle('Modern TV');
      const searchInput = screen.getByLabelText('Icon Search');

      fireEvent.change(searchInput, { target: { value: 'modern' } });

      expect(dramaIcon).not.toBeVisible();
      expect(modernTvIcon).toBeVisible();
    });
  });

  describe('As a moderator', () => {
    test('I can see a list of all the icons available to me, including moderator specific icons', async () => {
      await act(async () => {
        customRender(<IconSelector handleIconChange={handleIconChangeMock} />);
      });

      const dramaIcon = screen.getByTitle('Grocery');
      const healthIcon = screen.getByTitle('Health');
      const hotTopicIcon = screen.queryByTitle('Hot Topic');

      expect(dramaIcon).toBeDefined();
      expect(healthIcon).toBeDefined();
      expect(hotTopicIcon).toBeDefined();
    });
  });
});
