/* eslint-disable react/jsx-props-no-spreading */
import React from 'react';

import axios from 'axios';
import { waitFor } from '@testing-library/react';
import ModerationReportDetail from '../../../../src/views/Moderation/components/ModerationReportDetail';
import { customRender } from '../../../custom_renderer';

jest.mock('axios');

describe('ModerationReportDetail component', () => {
  let report = {};
  beforeEach(() => {
    report = {
      reason: 'things',
      post: {
        id: 1,
        createdAt: '2020-10-15T15:40:53.000Z',
        updatedAt: '2020-10-15T15:40:53.000Z',
        user: { id: 2, username: 'Rick', createdAt: '2019-03-30T03:13:23.000Z' },
        thread: { subforum: {} },
        bans: [],
      },
      reportedBy: { id: 3, username: 'Joe' },
      solvedBy: null,
    };
  });

  it('displays report controls', async () => {
    axios.get.mockResolvedValue({ data: report });
    const { queryByText, getByText } = customRender(<ModerationReportDetail />);
    await waitFor(() => {
      getByText('Activity');
    });
    expect(queryByText('Dismiss')).not.toBeNull();
    expect(queryByText('Ban user')).not.toBeNull();
  });

  it('hides report controls if report is closed', async () => {
    report.solvedBy = { id: 4, username: 'Bob' };
    axios.get.mockResolvedValue({ data: report });
    const { queryByText, getByText } = customRender(<ModerationReportDetail />);
    await waitFor(() => {
      getByText('Activity');
    });
    expect(queryByText('Dismiss')).toBeNull();
    expect(queryByText('Ban user')).toBeNull();
  });

  it('displays an event if report is closed', async () => {
    report.solvedBy = { id: 4, username: 'Bob' };
    axios.get.mockResolvedValue({ data: report });
    const { queryByText, getByText } = customRender(<ModerationReportDetail />);
    await waitFor(() => {
      getByText('Activity');
    });
    expect(queryByText('resolved this report')).not.toBeNull();
  });

  it('displays an event if the post is banned', async () => {
    report.post.bans = [
      { id: 1, banReason: 'stuff', bannedBy: { id: 4, username: 'Ron' }, user: report.post.user },
    ];
    axios.get.mockResolvedValue({ data: report });
    const { queryByText, getByText } = customRender(<ModerationReportDetail />);
    await waitFor(() => {
      getByText('Activity');
    });
    expect(queryByText('Ron')).not.toBeNull();
  });
});
