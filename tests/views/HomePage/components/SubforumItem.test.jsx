/* eslint-disable react/jsx-props-no-spreading */
import React from 'react';

import { screen } from '@testing-library/react';
import SubforumItem from '../../../../src/views/HomePage/components/SubforumItem';
import { customRender } from '../../../custom_renderer';
import '@testing-library/jest-dom/extend-expect';
import { BASIC_USER } from '../../../../src/utils/roleCodes';

const mockedProps = {
  index: 10,
  createdAt: '2011-03-04T00:13:01.000Z',
  description: 'For your daily existential crisis fuel dose',
  icon: 'https://img.icons8.com/color/96/000000/news.png',
  iconId: 6,
  id: 6,
  name: 'News',
  totalPosts: 128092,
  totalThreads: 5076,
  updatedAt: '2019-03-04T00:13:01.000Z',
  isPlaceholder: false,
  lastPostId: 890720,
  lastPost: {
    id: 890720,
    createdAt: '2021-06-21T04:41:05.000Z',
    user: {
      id: 2096,
      username: 'megafat',
      role: { code: BASIC_USER },
      avatarUrl: '2096.webp',
      backgroundUrl: '2096-bg.webp',
      posts: 2642,
      threads: 36,
      createdAt: '2019-05-04T06:47:46.000Z',
      updatedAt: '2019-05-04T06:47:46.000Z',
      banned: false,
      isBanned: false,
    },
    thread: {
      id: 1234,
      title: 'Incredibly long title that should be overflown',
      iconId: 84,
      subforumId: 6,
      createdAt: '2021-06-20T22:49:02.000Z',
      updatedAt: '2021-06-20T22:49:02.000Z',
      deletedAt: null,
      deleted: false,
      locked: false,
      pinned: false,
      subscribed: false,
      lastPost: {
        id: 123456,
        thread: 1234,
        page: 1,
        content: 'content :)',
        createdAt: '2021-06-21T04:41:05.000Z',
        updatedAt: '2021-06-21T04:41:05.000Z',
        user: {
          id: 1234,
          username: 'user',
          role: { code: BASIC_USER },
          avatarUrl: '1234.webp',
          backgroundUrl: '1234-bg.webp',
          posts: 1234,
          threads: 1234,
          createdAt: '2019-05-04T06:47:46.000Z',
          updatedAt: '2019-05-04T06:47:46.000Z',
          banned: false,
          isBanned: false,
        },
        ratings: [],
        bans: [],
        threadPostNumber: 5,
        countryName: null,
        countryCode: null,
        appName: 'knockout.chat',
      },
      backgroundUrl: null,
      backgroundType: null,
      user: {
        id: 1234,
        username: 'user',
        role: { code: BASIC_USER },
        avatarUrl: '1234.webp',
        backgroundUrl: '1234-bg.webp',
        posts: 1234,
        threads: 1234,
        createdAt: '2019-05-04T06:47:46.000Z',
        updatedAt: '2019-05-04T06:47:46.000Z',
        banned: false,
        isBanned: false,
      },
      postCount: 10,
      recentPostCount: 0,
      unreadPostCount: 0,
      readThreadUnreadPosts: 0,
      read: false,
      hasRead: false,
      hasSeenNoNewPosts: false,
      firstPostTopRating: {
        id: 123456,
        rating: 'sad',
        ratingId: 6,
        count: 40,
      },
      subforum: null,
      tags: [],
      viewers: {
        memberCount: 4,
        guestCount: 1,
      },
    },
  },
};

describe('SubforumItem Component', () => {
  it('has html "title" attribute for thread title', () => {
    customRender(<SubforumItem {...mockedProps} />);

    const expectedTitle = mockedProps.lastPost.thread.title;

    const threadTitle = screen.getByText(expectedTitle);
    expect(threadTitle).toHaveAttribute('title', expectedTitle);
  });
});
